//
//  CNLogViewController.m
//  AppRTC
//
//  Created by Saptarshi's  on 7/8/16.
//  Copyright © 2016 ISBX. All rights reserved.
//

#import "CNLogViewController.h"
#import "CNCallLogTableViewCell.h"
#import "AppDelegate.h"
#import "CContactDetailsViewController.h"

#define MISSEDCALL @"missedCall.png"
#define INCOMINGCALL @"incommingCall.png"
#define OUTGOINGCALL @"outgoingCall.png"

#define WEEKDAYARRAY [NSArray arrayWithObjects:@"Sunday",@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday", nil]

@interface CNLogViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *logArray;
    AppDelegate *appDel;
    NSIndexPath *indexpathSelected;
}
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UITableView *logTable;
@property (weak, nonatomic) IBOutlet UILabel *errorLBL;
@property (weak, nonatomic) IBOutlet UIButton *clearBTN;

@end

@implementation CNLogViewController

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(fetchCallLogWithType:) name:@"CallLogUpdated" object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    appDel=(AppDelegate *)[UIApplication sharedApplication].delegate;
    [self fetchCallLogWithType:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self addTabBarToView:self.view];
}

-(void)fetchCallLogWithType:(NSString *)callType
{
    @try {
        
        logArray = [NSMutableArray new];
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        
        [managedObjectContext performBlock:^{
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CallLog"];
            if ([callType isKindOfClass:[NSString class]] && [callType isEqualToString:@"0"])
            {
                NSPredicate *predicate=[NSPredicate predicateWithFormat:@"callType==%d",[[NSString stringWithFormat:@"%@",callType] intValue]]; // If required to fetch specific user
                fetchRequest.predicate=predicate;
            }
            NSError *error = nil;
            NSUInteger count = [managedObjectContext countForFetchRequest:fetchRequest
                                                                    error:&error];
            if (count>0)
            {
                NSMutableArray *list = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                NSLog(@"number of list : %d",(int)list.count);
                for (NSManagedObject *user in list)
                {
                    NSMutableDictionary *mutDic=[NSMutableDictionary new];
                    [mutDic setObject:[NSString stringWithFormat:@"%@",[user valueForKey:@"name"]] forKey:@"name"];
                    [mutDic setObject:(NSDate *)[user valueForKey:@"time"] forKey:@"time"];
                    [mutDic setObject:(NSNumber *)[user valueForKey:@"callType"] forKey:@"callType"];
                    [mutDic setObject:(NSDate *)[user valueForKey:@"dateString"] forKey:@"dateString"];
                    [mutDic setObject:(NSDate *)[user valueForKey:@"date"] forKey:@"date"];
                    [mutDic setObject:[user valueForKey:@"numberOfCall"] forKey:@"numberOfCall"];
                    [mutDic setObject:[NSString stringWithFormat:@"%@",[user valueForKey:@"phone"]] forKey:@"phone"];
                    
                    [logArray addObject:mutDic];
                }
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateString" ascending:FALSE];
                [logArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                [_logTable reloadData];
                [_logTable setHidden:NO];
                [self stopLoading];
                [_clearBTN setHidden:NO];
            }
            else
            {
                [_logTable setHidden:YES];
                [_errorLBL setHidden:NO];
                [_clearBTN setHidden:YES];
            }
        }];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark UITableVIewDataSource and UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return logArray.count;
}

- (CNCallLogTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CNCallLogTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"logCell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(CNCallLogTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IsIphone4||IsIphone5)
        [cell.nameLBL setFont:[UIFont systemFontOfSize:15.0f]];
    [cell.nameLBL setTextColor:[UIColor blackColor]];
    NSDictionary *contact;
    if ([logArray objectAtIndex:indexPath.row])
        contact = [logArray objectAtIndex:indexPath.row];
    
    [cell.timeLBL setText:[NSString stringWithFormat:@"%@",[self relativeDateStringForDate:(NSDate *)[contact valueForKey:@"dateString"]]]];
    if ([contact[@"callType"] integerValue] == 0)
    {
        [cell.nameLBL setTextColor:[UIColor redColor]];
        [cell.callTypeImage setImage:[UIImage imageNamed:MISSEDCALL]];
    }
    else if ([contact[@"callType"]integerValue] == 2)
        [cell.callTypeImage setImage:[UIImage imageNamed:INCOMINGCALL]];
    else if ([contact[@"callType"]integerValue] == 1)
        [cell.callTypeImage setImage:[UIImage imageNamed:OUTGOINGCALL]];
    if ([[contact valueForKey:@"numberOfCall"]intValue]==1)
        [cell.nameLBL setText:[contact valueForKey:@"name"]];
    else if ([[contact valueForKey:@"numberOfCall"]intValue]>1)
        [cell.nameLBL setText:[NSString stringWithFormat:@"%@ (%d)",[contact valueForKey:@"name"],[[contact valueForKey:@"numberOfCall"]intValue]]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
    indexpathSelected = indexPath;
    NSLog(@"index path row %ld section %ld",indexpathSelected.row,indexpathSelected.section);
    CContactDetailsViewController *CCDVC=[[UIStoryboard storyboardWithName:@"IPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"CContactDetailsViewController"];
    CCDVC.userPhoneNumber = [[logArray objectAtIndex:indexPath.row] valueForKey:@"phone"];
    CCDVC.isFromLog=YES;
    [self PushViewController:CCDVC WithAnimation:kCAMediaTimingFunctionEaseIn];
}

- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute;
    
    NSLog(@"two dates are : date %@ now %@",date,[NSDate date]);
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSCalendar *calendar = [[NSCalendar alloc]
                            initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:units fromDate:date toDate:[NSDate date]options:NSCalendarMatchStrictly];
    
    
    if (components.year > 0)
    {
        [appDel.dateFormatter setDateFormat:@"dd/MM/yy"];
        return [NSString stringWithFormat:@"%@", [appDel.dateFormatter stringFromDate:date]];
    }
    else if (components.month > 0)
    {
        [appDel.dateFormatter setDateFormat:@"dd/MM/yy"];
        return [NSString stringWithFormat:@"%@", [appDel.dateFormatter stringFromDate:date]];
    }
    else if (components.weekOfYear > 0)
    {
        [appDel.dateFormatter setDateFormat:@"dd/MM/yy"];
        return [NSString stringWithFormat:@"%@", [appDel.dateFormatter stringFromDate:date]];
    }
    else if (components.day > 0)
    {
        if (components.day > 1)
        {
            components = [calendar components:NSCalendarUnitWeekday fromDate:date];
            return [NSString stringWithFormat:@"%@", WEEKDAYARRAY[components.weekday-1]];
        }
        else
        {
            return @"Yesterday";
        }
    } else
    {
        [appDel.dateFormatter setDateFormat:@"hh:mm a"];
        return [appDel.dateFormatter stringFromDate:date];
    }
}

- (IBAction)clearLog:(id)sender
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Alert!"
                                          message:[NSString stringWithFormat:@"Are you sure wants to clear log?"]
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *yesAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Yes", @"YES action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
                                   
                                   [ managedObjectContext performBlock:^{
                                       
                                       NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                                       NSEntityDescription *entity = [NSEntityDescription entityForName:@"CallLog" inManagedObjectContext:managedObjectContext];
                                       [fetchRequest setEntity:entity];
                                       
                                       NSError *error;
                                       NSArray *items = [ managedObjectContext executeFetchRequest:fetchRequest error:&error];
                                       
                                       DebugLog(@"count of data %d",(int)items.count );
                                       
                                       for (NSManagedObject *managedObject in items)
                                       {
                                           [managedObjectContext deleteObject:managedObject];
                                           DebugLog(@"coredata object deleted");
                                       }
                                       if (![managedObjectContext save:&error])
                                       {
                                           DebugLog(@"Error deleting coredata - error:%@",error);
                                       }
                                       [self fetchCallLogWithType:nil];
                                       
                                   }];
                               }];
    UIAlertAction *noAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"No", @"NO action")
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction *action)
                               {
                               }];
    [alertController addAction:yesAction];
    [alertController addAction:noAction];
    [self presentViewController:alertController animated:YES completion:nil];;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    if ([logArray count]>0)
    {
        if (indexpathSelected != nil && indexpathSelected.row != NSNotFound && indexpathSelected.section != NSNotFound)
        {
            [[_logTable cellForRowAtIndexPath:indexpathSelected] setBackgroundColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:1.0f]];
            [_logTable reloadRowsAtIndexPaths:@[indexpathSelected] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}
- (IBAction)segmentValueChange:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == 0)
    {
        [self fetchCallLogWithType:nil];
    }
    else if (sender.selectedSegmentIndex == 1)
    {
        [self fetchCallLogWithType:[NSString stringWithFormat:@"0"]];
    }
}

@end
