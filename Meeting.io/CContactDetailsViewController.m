//
//  CContactDetailsViewController.m
//  Connex-New
//
//  Created by Saptarshi's  on 6/25/16.
//  Copyright © 2016 esolz. All rights reserved.
//

#import "CContactDetailsViewController.h"
#import "UIImageView+WebCache.h"
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"
#import "ARTCVideoChatViewController.h"
#import <Contacts/Contacts.h>
#import "JTSImageInfo.h"
#import "JTSImageViewController.h"
#import "CContactListViewController.h"
#import "KeychainItemWrapper.h"


@interface CContactDetailsViewController ()<MFMessageComposeViewControllerDelegate, UIAlertViewDelegate, AVAudioSessionDelegate>
{
    NSArray *phoneNumArray,*emailArray;
    NSString *contactID;
    NSString *contactName;
    NSOperationQueue *opertaionQueue;
    AppDelegate *appDel;
    NSString *callType;
    int timeTaken,buttonTag;
    UIImage *profileImage;
    NSString *connexUser;
    NSMutableDictionary *userDic;
    NSManagedObject *userDBObject;
    NSMutableArray *connexIdArray,*urlValueArray;
    NSString *roomID;
}
@property (weak, nonatomic) IBOutlet UIView *phonuNumberContainerView;
@property (weak, nonatomic) IBOutlet UIView *favoriteView;
//@property (weak, nonatomic) IBOutlet UIView *connexView;
//@property (weak, nonatomic) IBOutlet UIView *connexVoiceView;
//@property (weak, nonatomic) IBOutlet UIView *connexVideoView;
//@property (weak, nonatomic) IBOutlet UILabel *connexVoiceLBL;
//@property (weak, nonatomic) IBOutlet UILabel *connexVideoLBL;
@property (weak, nonatomic) IBOutlet UIButton *backBTN;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *subContainer;
@property (weak, nonatomic) IBOutlet UIButton *connexAudioBTN;
@property (weak, nonatomic) IBOutlet UIButton *connexVideoBTN;
//@property (weak, nonatomic) IBOutlet UIButton *nomalCallIBTN;
//@property (weak, nonatomic) IBOutlet UIButton *normalMsgBTN;
//@property (weak, nonatomic) IBOutlet UIView *inviteBackView;
//@property (weak, nonatomic) IBOutlet UIButton *inviteBTN;
@property (weak, nonatomic) IBOutlet UILabel *mobileLBL;

@end

@implementation CContactDetailsViewController

- (NSString *) timeStamp {
    return [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    userDic=[NSMutableDictionary new];
    buttonTag =-1;
    appDel = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [self setRoundCornertoView:_profilePic withBorderColor:nil borderWidth:0.0f WithRadius:.5 dependsOnHeight:YES];
    opertaionQueue=[[NSOperationQueue alloc]init];
    int y = _profilePic.frame.origin.y + (_profilePic.frame.size.height /2);
    [_nameLBL setFrame:CGRectMake(_nameLBL.frame.origin.x, y-_nameLBL.frame.size.height/2, _nameLBL.frame.size.width, _nameLBL.frame.size.height)];
    
    [_profilePic setFrame:CGRectMake(_profilePic.frame.origin.x, _profilePic.frame.origin.y, _profilePic.frame.size.height, _profilePic.frame.size.height)];
    
    
    [_backBTN.titleLabel setFont:[UIFont fontWithName:_backBTN.titleLabel.font.fontName size:[self setFontSize:_backBTN.titleLabel.font.pointSize]]];
    
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"ContactList"];
    NSArray *arr=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for (int i=0 ; i<arr.count ; i++)
    {
        NSManagedObject *obj=[arr objectAtIndex:i];
        NSArray *phoneArray = [[obj valueForKey:@"phone"]mutableCopy];
        for (NSString *str in phoneArray)
        {
            if ([str rangeOfString:[NSString stringWithFormat:@"%@",_userPhoneNumber]].location != NSNotFound)
            {
                userDBObject = obj;
                break;
            }
        }
    }
    
    if (userDBObject != nil)
    {
        [userDic setObject:[NSString stringWithFormat:@"%@",[userDBObject valueForKey:@"name"]] forKey:@"name"];
        [userDic setObject:[NSString stringWithFormat:@"%@",[userDBObject valueForKey:@"firstName"]] forKey:@"firstName"];
        [userDic setObject:[NSString stringWithFormat:@"%@",[userDBObject valueForKey:@"lastName"]] forKey:@"lastName"];
        
        [userDic setObject:(NSArray *)[userDBObject valueForKey:@"phone"] forKey:@"phone"];
        [userDic setObject:[NSString stringWithFormat:@"%@",[userDBObject valueForKey:@"image"]] forKey:@"image"];
        [userDic setObject:(NSArray *)[userDBObject valueForKey:@"email"] forKey:@"email"];
        [userDic setObject:[userDBObject valueForKey:@"id"] forKey:@"id"];
        [userDic setObject:[userDBObject valueForKey:@"connexUser"] forKey:@"connexUser"];
        [userDic setObject:(NSArray *)[userDBObject valueForKey:@"connexID"] forKey:@"connexID"];
        NSMutableArray *postalAddress=[NSMutableArray new];
        if ([userDBObject valueForKey:@"postalAddress"] != nil)
            postalAddress = [[userDBObject valueForKey:@"postalAddress"] mutableCopy];
        [userDic setObject:postalAddress forKey:@"postalAddress"];
    }
    
    emailArray = [userDic objectForKey:@"email"];
    phoneNumArray =[[userDic objectForKey:@"phone"]mutableCopy];
    
    NSFetchRequest *connexFetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"ConnexList"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"id==%@ AND typeOfConnexUser==%@",[NSString stringWithFormat:@"%@",[userDBObject valueForKey:@"id"]],@"F"]; // If required to fetch specific user
    connexFetchRequest.predicate=predicate;
    NSArray *userArr=[[self.managedObjectContext executeFetchRequest:connexFetchRequest error:nil] mutableCopy];
    NSManagedObject *user=[userArr lastObject];
    if (user == nil && [phoneNumArray count]>0)
    {
        [self startLoadingOnView:self.view];
        NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSMutableArray *tempPhoneArray=[NSMutableArray new];
        for (NSString *str in phoneNumArray)
        {
            NSString *subStr;
            if([str length]>=10)
                subStr=[str substringFromIndex:[str length]-10];
            else
                subStr=str;
            NSString *resultString = [[subStr componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
            NSLog (@"Result: %@", resultString);
            [tempPhoneArray addObject:resultString];
        }
        connexUser=@"NO";
        [opertaionQueue addOperationWithBlock:^{
            [self syncContactToServer:tempPhoneArray];
        }];
    }
    else
    {
        connexUser=@"YES";
        urlValueArray=[NSMutableArray new];
        NSArray *idArr=[user valueForKey:@"connexID"];
        if ([idArr count] == [phoneNumArray count])
        {
            for (int i=0;i<[phoneNumArray count];i++)
            {
                NSMutableDictionary *dic=[NSMutableDictionary new];
                dic[@"id"]=idArr[i];
                dic[@"connex"]=@"YES";
                [urlValueArray addObject:dic];
            }
            [self stopLoading];
            [self setupViews];
        }
        else
        {
            [self startLoadingOnView:self.view];
            NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
            NSMutableArray *tempPhoneArray=[NSMutableArray new];
            for (NSString *str in phoneNumArray)
            {
                NSString *subStr;
                if([str length]>=10)
                    subStr=[str substringFromIndex:[str length]-10];
                else
                    subStr=str;
                NSString *resultString = [[subStr componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
                NSLog (@"Result: %@", resultString);
                [tempPhoneArray addObject:resultString];
            }
            connexUser=@"NO";
            [opertaionQueue addOperationWithBlock:^{
                [self syncContactToServer:tempPhoneArray];
            }];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self addTabBarToView:self.view];
    NSString *filePath;
    NSString *searchFilename = [NSString stringWithFormat:@"pic%@.png",userDic[@"id"]]; // name of the PDF you are searching for
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSDirectoryEnumerator *direnum = [[NSFileManager defaultManager] enumeratorAtPath:documentsDirectory];
    
    NSString *documentsSubpath;
    while (documentsSubpath = [direnum nextObject])
    {
        if (![documentsSubpath.lastPathComponent isEqual:searchFilename]) {
            continue;
        }
        filePath = [NSString stringWithFormat:@"%@/%@",documentsDirectory,documentsSubpath];
        NSLog(@"found %@/%@", documentsDirectory,documentsSubpath);
    }
    
    if ([userDic valueForKey:@"id"])
    {
        [_nameLBL setText:[NSString stringWithFormat:@"%@",[userDic objectForKey:@"name"]]];
        contactName = [NSString stringWithFormat:@"%@",[userDic objectForKey:@"name"]];
        if ([filePath length]>0)
        {
            profileImage =[UIImage imageWithContentsOfFile:filePath];
            [_profilePic setImage:profileImage];
            UITapGestureRecognizer *tapOnProPic =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewImage:)];
            [tapOnProPic setNumberOfTapsRequired:1];
            [_profilePic addGestureRecognizer:tapOnProPic];
        }
        else
        {
            NSMutableString *initisals=[[NSMutableString alloc]init];
            if ([[userDic objectForKey:@"name"] length] > 0) {
                NSString * firstLetter = [[userDic objectForKey:@"name"] substringWithRange:[[userDic objectForKey:@"name"] rangeOfComposedCharacterSequenceAtIndex:0]];
                [initisals appendString:[firstLetter uppercaseString]];
            }
            UILabel *nameLabel=[[UILabel alloc]initWithFrame:_profilePic.frame];
            [nameLabel setBackgroundColor:[UIColor lightGrayColor]];
            [nameLabel setTextColor:[UIColor whiteColor]];
            [nameLabel setFont:[UIFont systemFontOfSize:25]];
            [nameLabel setTextAlignment:NSTextAlignmentCenter];
            [_mainScroll addSubview:nameLabel];
            [nameLabel setText:initisals];
            [self setRoundCornertoView:nameLabel withBorderColor:nil borderWidth:0 WithRadius:.5 dependsOnHeight:YES];
        }
    }
    else if (userDBObject == nil)
    {
        [self startLoadingOnView:self.view];
        [_nameLBL setText:@"No name"];
        [_favoriteView setHidden:YES];
        phoneNumArray =[ NSArray arrayWithObject:_userPhoneNumber];
        NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSMutableArray *tempPhoneArray=[NSMutableArray new];
        for (NSString *str in phoneNumArray)
        {
            NSString *subStr;
            if([str length]>=10)
                subStr=[str substringFromIndex:[str length]-10];
            else
                subStr=str;
            NSString *resultString = [[subStr componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
            NSLog (@"Result: %@", resultString);
            [tempPhoneArray addObject:resultString];
        }
        connexUser=@"NO";
        [opertaionQueue addOperationWithBlock:^{
            [self syncContactToServer:tempPhoneArray];
        }];
    }
}

-(void)viewImage:(UIGestureRecognizer *)recognizer
{
    // Create image info
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    imageInfo.image = _profilePic.image;
    imageInfo.referenceRect = _profilePic.frame;
    imageInfo.referenceView = _profilePic.superview;
    
    // Setup view controller
    
    
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                           initWithImageInfo:imageInfo
                                           mode:JTSImageViewControllerMode_Image
                                           backgroundStyle:JTSImageViewControllerBackgroundOption_Scaled];
    
    // Present the view controller.
    [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];
}

-(void)voiceCall:(UIButton *)sender
{
    contactID=[[urlValueArray objectAtIndex:sender.tag] valueForKey:@"id"];
    NSString *phnNum=[phoneNumArray objectAtIndex:sender.tag];
    if ([[self checkStatusForMicrophoneAndCameraPermission]isEqualToString:@"OK"])
    {
        callType=@"voice";
        [self setupDiallerWithCallType:callType onNumber:phnNum];
    }
    else
    {
        [self showAlertForCameraAndMicrophonePermissions];
    }
}

-(void)videoCall:(UIButton *)sender
{
    contactID=[[urlValueArray objectAtIndex:sender.tag] valueForKey:@"id"];
    NSString *phnNum=[phoneNumArray objectAtIndex:sender.tag];
    if ([[self checkStatusForMicrophoneAndCameraPermission]isEqualToString:@"OK"])
    {
        callType=@"video";
        [self setupDiallerWithCallType:callType onNumber:phnNum];
    }
    else
    {
        [self showAlertForCameraAndMicrophonePermissions];
    }
}

-(void)setupDiallerWithCallType:(NSString *)typeOfCall onNumber:(NSString *)phone
{
//    KeychainItemWrapper *keychainItem=keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:CNPHONE accessGroup:nil];
    NSString *ownPhoneNum = [[NSUserDefaults standardUserDefaults] objectForKey:CNPHONE];//[keychainItem objectForKey:(__bridge id)(kSecAttrAccount)];
    if ([phone rangeOfString:[NSString stringWithFormat:@"%@",ownPhoneNum]].location == NSNotFound)
    {
        [appDel.dateFormatter setDateFormat:@"hh:mm a"];
        NSDate *time = [appDel.dateFormatter dateFromString:[appDel.dateFormatter stringFromDate:[NSDate date]]];
        [appDel.dateFormatter setDateFormat:@"dd-MM-YYYY"];
        NSString *date = [appDel.dateFormatter stringFromDate:[NSDate date]];
        
        if (contactName==nil)
            contactName=_userPhoneNumber;
        [self addToCallLogWithName:contactName phone:phone typeOfCall:[NSNumber numberWithInt:1] time:time date:date fullDate:[NSDate date]];
        
        appDel.outgoingCallView=[[[NSBundle mainBundle]loadNibNamed:@"Dialler" owner:self options:nil]objectAtIndex:1];
        [appDel.outgoingCallView setAlpha:0.0];
        [appDel.outgoingCallView setFrame:CGRectMake(0, 0, FULLWIDTH, FULLHEIGHT)];
        [self.view addSubview:appDel.outgoingCallView];
        [UIView animateWithDuration:0.2 animations:^{
            [appDel.outgoingCallView setAlpha:1.0f];
        }];
        
        UIImageView *backImage=(UIImageView *)[appDel.outgoingCallView viewWithTag:1];
        [backImage setBackgroundColor:[UIColor clearColor]];
        if (profileImage != nil)
            [backImage setImage:profileImage];
        
        UILabel *nameLBL=(UILabel *)[appDel.outgoingCallView viewWithTag:2];
        [nameLBL setText:[contactName capitalizedString]];
        
        UILabel *callTypeLBL = (UILabel *)[appDel.outgoingCallView viewWithTag:3];
        [callTypeLBL setText:[NSString stringWithFormat:@"Connex %@ Call",[typeOfCall capitalizedString]]];
        
        UIButton *endBTN=(UIButton *)[appDel.outgoingCallView viewWithTag:8];
        [self setRoundCornertoView:endBTN withBorderColor:nil borderWidth:0.0f WithRadius:.5 dependsOnHeight:YES];
        
        [endBTN addTarget:self action:@selector(callResponse:) forControlEvents:UIControlEventTouchUpInside];
        NSError *error= nil;
        
        if ([[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:&error])
        {}
        
        NSLog(@"error %@",error.localizedDescription);
        
        NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Phone_Ring" ofType: @"mp3"];
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath ];
        
        
        appDel.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
        appDel.audioPlayer.numberOfLoops = 1; //infinite loop
        [appDel.audioPlayer play];
        
        [[AVAudioSession sharedInstance]  overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:nil];
        
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
        [[AVAudioSession sharedInstance] setActive:YES error:nil];
        
        
        UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_None;
        
        AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute,
                                 sizeof (audioRouteOverride),
                                 &audioRouteOverride);
        roomID=[self timeStamp];
        [self sendPushToUser:contactID withRoomID:roomID withCallType:typeOfCall withStatus:@""];//calling
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDel.callTimer invalidate];
            appDel.callTimer = nil;
            timeTaken=0;
            appDel.callTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkCall:) userInfo:nil repeats:YES];
        });
        
        NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
        NSString *subStr;
        if([phone length]>=10)
            subStr=[phone substringFromIndex:[phone length]-10];
        else
            subStr=phone;
        NSString *resultString = [[subStr componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
        appDel.callerNumber=resultString;
//        [appDel testAlert1:contactID];
        [[NSUserDefaults standardUserDefaults]setObject:connexCallStatusCalling forKey:connexCallStatus];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Alert!"
                                              message:[NSString stringWithFormat:@"You can not call to your own number."]
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                   }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)checkCall:(NSTimer *)timer
{
    timeTaken+=1;
    if (timeTaken == RINGINGTIME)
    {
        NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"phone-busy" ofType: @"mp3"];
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath ];
        
        
        appDel.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
        appDel.audioPlayer.numberOfLoops = 1; //infinite loop
        [appDel.audioPlayer play];
        
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self sendPushToUser:contactID withRoomID:roomID withCallType:@"callEnded" withStatus:@""];//callEnded
            [UIView animateWithDuration:0.1 animations:^{
                [appDel.outgoingCallView setAlpha:0.0];
            } completion:^(BOOL finished) {
                if (finished)
                {
                    [appDel.callTimer invalidate];
                    appDel.callTimer = nil;
                    appDel.endedMeetingID=roomID;
                    [appDel.audioPlayer stop];
                    [appDel.outgoingCallView removeFromSuperview];
                    appDel.outgoingCallView=nil;
                    [[NSUserDefaults standardUserDefaults]setObject:connexCallStatusIdle forKey:connexCallStatus];
                }
            }];
        });
    }
}

-(void)callResponse:(UIButton *)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDel.callTimer invalidate];
        appDel.callTimer = nil;
    });
    [self sendPushToUser:contactID withRoomID:roomID withCallType:@"callEnded" withStatus:@""];//callEnded
    [UIView animateWithDuration:0.1 animations:^{
        [appDel.outgoingCallView setAlpha:0.0];
    } completion:^(BOOL finished) {
        if (finished)
        {
            appDel.endedMeetingID=roomID;
            [appDel.audioPlayer stop];
            [appDel.outgoingCallView removeFromSuperview];
            appDel.outgoingCallView=nil;
            [[NSUserDefaults standardUserDefaults]setObject:connexCallStatusIdle forKey:connexCallStatus];
        }
    }];
}

-(void)handleSwipeFrom:(UIGestureRecognizer *)recogniser
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)syncContactToServer:(NSArray *)phoneArray
{    
    NSError *localErr;
    
    NSHTTPURLResponse *response = nil;
    
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:phoneArray options:NSJSONWritingPrettyPrinted error:&localErr];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    
    DebugLog(@"json string ertta: %@",jsonString);
    
    
    NSMutableString *urlString=[NSMutableString stringWithFormat:@"%@/fetch_user_v2?details_info=%@",DOMAINURL,[self encodeToPercentEscapeString:jsonString]];
    
    NSLog(@"url string %@",urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    //-- Get request and response though URL
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //-- JSON Parsing
    NSMutableDictionary *result;
    if (responseData != nil)
        result = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&localErr];
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self stopLoading];
        });
    }
    NSLog(@"Result = %@",result);
    if (result != nil)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([[result objectForKey:@"response"]isEqualToString:@"TRUE"] || [[result objectForKey:@"info"]count]>0)
            {
                urlValueArray = [[result objectForKey:@"info"] mutableCopy];
                connexIdArray=[NSMutableArray new];
                for (NSDictionary *dic in urlValueArray)
                {
                    if ([dic[@"id"]length]>0)
                        [connexIdArray addObject:dic[@"id"]];
                    else
                        [connexIdArray addObject:@""];
                }
                [self stopLoading];
                
                connexUser=@"YES";
                
                NSLog(@"phone %@",[userDBObject valueForKey:@"phone"]);
                [userDBObject setValue:connexUser forKey:@"connexUser"];
                [userDBObject setValue:connexIdArray forKey:@"connexID"];
                NSError *error=nil;
                if (![self.managedObjectContext save:&error])
                {
                    NSLog(@"error %@",error.localizedDescription);
                }
                
                NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"ConnexList"];
                NSPredicate *predicate=[NSPredicate predicateWithFormat:@"id==%@",[NSString stringWithFormat:@"%@",[userDBObject valueForKey:@"id"]]]; // If required to fetch specific user
                fetchRequest.predicate=predicate;
                NSArray *userArr=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                NSManagedObject *user=[userArr lastObject];
                if (user !=nil)
                {
                    if ([phoneNumArray count]==[connexIdArray count])
                        [user setValue:@"F" forKey:@"typeOfConnexUser"];
                    else
                        [user setValue:@"P" forKey:@"typeOfConnexUser"];
                    [user setValue:connexIdArray forKey:@"connexID"];
                }
                else if (userDBObject !=nil)
                {
                    @try
                    {
                        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
                        [managedObjectContext performBlock:^{
                            NSManagedObject *newObject = [NSEntityDescription insertNewObjectForEntityForName:@"ConnexList" inManagedObjectContext:managedObjectContext];
                            if ([phoneNumArray count]==[connexIdArray count])
                                [newObject setValue:@"F" forKey:@"typeOfConnexUser"];
                            else
                                [newObject setValue:@"P" forKey:@"typeOfConnexUser"];
                            [newObject setValue:[userDBObject valueForKey:@"id"] forKey:@"id"];
                            [newObject setValue:connexIdArray forKey:@"connexID"];
                            NSError *error = nil;
                            if ([managedObjectContext save:&error])
                            {   }
                        }];
                    } @catch (NSException *exception)
                    {   }
                    @finally
                    {   }
                }
            }
            else
            {
                [self stopLoading];
            }
            [self setupViews];
        });
    }
}

-(void)setupViews
{
    NSFetchRequest *favFetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"FavoriteList"];
    NSPredicate *favPredicate=[NSPredicate predicateWithFormat:@"id==%@",[userDic objectForKey:@"id"]]; // If required to fetch specific user
    favFetchRequest.predicate=favPredicate;
    
    NSArray *favUser=[self.managedObjectContext executeFetchRequest:favFetchRequest error:nil];
    if ([favUser count]>0)
        [_favoriteView setHidden:YES];
    else
        [_favoriteView setHidden:NO];
    
    int position = 0;
    
    
    [_containerView setAutoresizesSubviews:NO];
    [_subContainer setAutoresizesSubviews:NO];
    if ([phoneNumArray count]>0)
    {
        int yposition=0,height=0;
        for (int i=0; i<[phoneNumArray count]; i++)
        {
            buttonTag++;
            UIView *backContainer=[[UIView alloc]initWithFrame:CGRectMake(0, yposition, FULLWIDTH, 40)];
            [backContainer setBackgroundColor:[UIColor clearColor]];
            
            UILabel *phoneFieldLBL=[[UILabel alloc]initWithFrame:CGRectMake(_mobileLBL.frame.origin.x, 0, FULLWIDTH/2, backContainer.frame.size.height)];
            [phoneFieldLBL setFont:[UIFont systemFontOfSize:17.0f]];
            [backContainer addSubview:phoneFieldLBL];
            [phoneFieldLBL setText:phoneNumArray[i]];
            [phoneFieldLBL setTextColor:[UIColor blackColor]];
            
//            UIView *divider=[[UIView alloc]initWithFrame:CGRectMake(_mobileLBL.frame.origin.x, backContainer.frame.size.height-1, FULLWIDTH-_mobileLBL.frame.origin.x, 1)];
//            [divider setBackgroundColor:[UIColor lightGrayColor]];
//            [divider setAlpha:0.5];
//            [backContainer addSubview:divider];
            [_phonuNumberContainerView addSubview:backContainer];
            UIView *tempView;
            if (i < [urlValueArray count])
            {
                NSString *connex = [[urlValueArray objectAtIndex:i] valueForKey:@"connex"];
                if ([connex isEqualToString:@"YES"])
                    tempView=[self createConnexViewAtY:backContainer.frame.origin.y+backContainer.frame.size.height];
                else
                    tempView = [self createInviteViewAtY:backContainer.frame.origin.y+backContainer.frame.size.height];
                [_phonuNumberContainerView addSubview:tempView];
            }
            else
            {
                tempView = [self createInviteViewAtY:backContainer.frame.origin.y+backContainer.frame.size.height];
                [_phonuNumberContainerView addSubview:tempView];
                
                UIButton *callBTN = [[UIButton alloc]initWithFrame:CGRectMake(FULLWIDTH-120, (backContainer.frame.size.height-32)/2,33, 32)];
                [callBTN setImage:[UIImage imageNamed:@"phone"] forState:UIControlStateNormal];
                [callBTN setTag:buttonTag];
                [backContainer addSubview:callBTN];
                
                UIButton *msgBTN = [[UIButton alloc]initWithFrame:CGRectMake(FULLWIDTH-60,(backContainer.frame.size.height-32)/2,33, 32)];
                [msgBTN setImage:[UIImage imageNamed:@"message_icon"] forState:UIControlStateNormal];
                [msgBTN setTag:buttonTag];
                [backContainer addSubview:msgBTN];
                
                [callBTN addTarget:self action:@selector(callToNumber:) forControlEvents:UIControlEventTouchUpInside];
                [msgBTN addTarget:self action:@selector(messageToNumber:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            yposition +=_phonuNumberContainerView.frame.size.height+tempView.frame.size.height+5;
            height +=backContainer.frame.size.height+tempView.frame.size.height;
        }
        [_phonuNumberContainerView setFrame:CGRectMake(0, _phonuNumberContainerView.frame.origin.y, FULLWIDTH, height)];
    }
    [_subContainer setFrame:CGRectMake(_subContainer.frame.origin.x, _phonuNumberContainerView.frame.size.height, _subContainer.frame.size.width, _subContainer.frame.size.height)];
    if ([emailArray count]>0)
    {
        UILabel *emailLBL=[[UILabel alloc]initWithFrame:CGRectMake(_mobileLBL.frame.origin.x, position, 127, 21)];
        [emailLBL setTextColor:TEXTCOLOR];
        [emailLBL setText:@"email"];
        [emailLBL setFont:[UIFont systemFontOfSize:14.0f]];
        [_subContainer addSubview:emailLBL];
        int yposition= emailLBL.frame.origin.y+emailLBL.frame.size.height+5;
        
        for (int i=0; i<[emailArray count]; i++)
        {
            UIView *backContainer=[[UIView alloc]initWithFrame:CGRectMake(0, yposition+5, FULLWIDTH, 40)];
            [backContainer setBackgroundColor:[UIColor clearColor]];
            UILabel *emailFieldLBL=[[UILabel alloc]initWithFrame:CGRectMake(_mobileLBL.frame.origin.x, 0, FULLWIDTH - _mobileLBL.frame.origin.x-10, backContainer.frame.size.height)];
            [emailFieldLBL setFont:[UIFont systemFontOfSize:17.0f]];
            [backContainer addSubview:emailFieldLBL];
            [emailFieldLBL setText:emailArray[i]];
            UIView *divider=[[UIView alloc]initWithFrame:CGRectMake(_mobileLBL.frame.origin.x, backContainer.frame.size.height-1, FULLWIDTH-_mobileLBL.frame.origin.x, 1)];
            [divider setBackgroundColor:[UIColor lightGrayColor]];
            [divider setAlpha:0.5];
            [backContainer addSubview:divider];
            [_subContainer addSubview:backContainer];
            yposition +=backContainer.frame.size.height;
        }
        position = yposition;
    }
    NSArray * address = [userDic objectForKey:@"postalAddress"];
    if (address != nil)
    {
        if ([address count]>0)
        {
            UILabel *addressLBL=[[UILabel alloc]initWithFrame:CGRectMake(_mobileLBL.frame.origin.x, position+10, 127, 21)];
            [addressLBL setTextColor:TEXTCOLOR];
            [addressLBL setText:@"address"];
            [addressLBL setFont:[UIFont systemFontOfSize:14.0f]];
            [_subContainer addSubview:addressLBL];
            int yposition= addressLBL.frame.origin.y + addressLBL.frame.size.height+5;
            for (int i=0; i<[address count]; i++)
            {
                UIView *backContainer=[[UIView alloc]initWithFrame:CGRectMake(0, yposition+5, FULLWIDTH, 30)];
                [backContainer setBackgroundColor:[UIColor clearColor]];
                UILabel *addressFieldLBL=[[UILabel alloc]initWithFrame:CGRectMake(_mobileLBL.frame.origin.x, 0, FULLWIDTH - _mobileLBL.frame.origin.x-10, 30)];
                [addressFieldLBL setFont:[UIFont systemFontOfSize:17.0f]];
                [backContainer addSubview:addressFieldLBL];
                [addressFieldLBL setNumberOfLines:0];
                [addressFieldLBL setLineBreakMode:NSLineBreakByWordWrapping];
                [addressFieldLBL setBackgroundColor:[UIColor clearColor]];
                NSMutableString *addressString = [[NSMutableString alloc]init];
                CNLabeledValue *addLBLValue = address[0];
                NSLog(@"addres value %@",[[addLBLValue value] valueForKey:@"street"]);
                (![[[addLBLValue value] valueForKey:@"street"] isEqual:@""] || ![[[addLBLValue value] valueForKey:@"street"]isKindOfClass:[NSNull class]] || ![[[addLBLValue value] valueForKey:@"street"] isEqual:@"(null)"])?[addressString appendString:[NSString stringWithFormat:@"%@\n",[[[addLBLValue value] valueForKey:@"street"] stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]]]]:[addressString appendString:@""];
                
                (![[[addLBLValue value] valueForKey:@"city"] isEqual:@""] || ![[[addLBLValue value] valueForKey:@"city"]isKindOfClass:[NSNull class]] || ![[[addLBLValue value] valueForKey:@"city"] isEqual:@"(null)"])?[addressString appendString:[NSString stringWithFormat:@"%@\n",[[addLBLValue value] valueForKey:@"city"]]]:[addressString appendString:@""];
                
                (![[[addLBLValue value] valueForKey:@"state"] isEqual:@""] || ![[[addLBLValue value] valueForKey:@"state"]isKindOfClass:[NSNull class]] || ![[[addLBLValue value] valueForKey:@"state"] isEqual:@"(null)"])?[addressString appendString:[NSString stringWithFormat:@"%@ ",[[addLBLValue value] valueForKey:@"state"]]]:[addressString appendString:@""];
                
                (![[[addLBLValue value] valueForKey:@"postalCode"] isEqual:@""] || ![[[addLBLValue value] valueForKey:@"postalCode"]isKindOfClass:[NSNull class]] || ![[[addLBLValue value] valueForKey:@"postalCode"] isEqual:@"(null)"])?[addressString appendString:[NSString stringWithFormat:@"%@\n",[[addLBLValue value] valueForKey:@"postalCode"]]]:[addressString appendString:@""];
                
                (![[[addLBLValue value] valueForKey:@"country"] isEqual:@""] || ![[[addLBLValue value] valueForKey:@"country"]isKindOfClass:[NSNull class]] || ![[[addLBLValue value] valueForKey:@"country"] isEqual:@"(null)"])?[addressString appendString:[NSString stringWithFormat:@"%@",[[addLBLValue value] valueForKey:@"country"]]]:[addressString appendString:@""];
                
                CGSize labelSize = [self sizeOfLabelwithFont:addressFieldLBL.font withText:addressString ofWidth:addressFieldLBL.frame.size.width];
                [addressFieldLBL setFrame:CGRectMake(_mobileLBL.frame.origin.x, 0, FULLWIDTH - _mobileLBL.frame.origin.x-10, labelSize.height)];
                [backContainer setFrame:CGRectMake(0, yposition+5, FULLWIDTH, labelSize.height+5)];
                [addressFieldLBL setText:addressString];
                
                UIView *divider=[[UIView alloc]initWithFrame:CGRectMake(_mobileLBL.frame.origin.x, backContainer.frame.size.height-1, FULLWIDTH-_mobileLBL.frame.origin.x, 1)];
                [divider setBackgroundColor:[UIColor lightGrayColor]];
                [divider setAlpha:0.5];
                [backContainer addSubview:divider];
                [_subContainer addSubview:backContainer];
                yposition +=backContainer.frame.size.height;
            }
            position = yposition;
        }
    }
    [_favoriteView setFrame:CGRectMake(0, position, FULLWIDTH, _favoriteView.frame.size.height+10)];
    
    [_subContainer setFrame:CGRectMake(0, _phonuNumberContainerView.frame.origin.y+_phonuNumberContainerView.frame.size.height, FULLWIDTH, _favoriteView.frame.origin.y+_favoriteView.frame.size.height)];
    
    [_containerView setFrame:CGRectMake(0, _containerView.frame.origin.y, FULLWIDTH, _subContainer.frame.origin.y+_subContainer.frame.size.height)];
    [_mainScroll setContentSize:CGSizeMake(FULLWIDTH, _containerView.frame.origin.y+_containerView.frame.size.height+10)];
    [_subContainer setHidden:NO];
}

-(UIView *)createConnexViewAtY:(CGFloat)yposition
{
    UIView *backContainer = [[UIView alloc]initWithFrame:CGRectMake(0, yposition, FULLWIDTH, 100)];

    UIView *audioView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, FULLWIDTH, backContainer.frame.size.height/2)];
    UIButton *audioBTN = [[UIButton alloc]initWithFrame:CGRectMake(_mobileLBL.frame.origin.x, 0, FULLWIDTH-_mobileLBL.frame.origin.x, audioView.frame.size.height)];
    [audioBTN setTitle:@"Connex Voice Call" forState:UIControlStateNormal];
    [audioBTN setTitleColor:TEXTCOLOR forState:UIControlStateNormal];
    audioBTN.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [audioBTN setTag:buttonTag];
    [audioBTN.titleLabel setFont:[UIFont systemFontOfSize:17.0f]];
    [audioView addSubview:audioBTN];
    UIView *divider=[[UIView alloc]initWithFrame:CGRectMake(_mobileLBL.frame.origin.x, audioView.frame.size.height-1, FULLWIDTH-_mobileLBL.frame.origin.x, 1)];
    [divider setBackgroundColor:[UIColor lightGrayColor]];
    [divider setAlpha:0.5];
    [audioView addSubview:divider];
    UIImageView *audioImgV = [[UIImageView alloc]initWithFrame:CGRectMake(audioView.frame.size.width - 50, (audioView.frame.size.height-32)/2, 33, 32)];
    [audioImgV setImage:[UIImage imageNamed:@"phone"]];
    [audioView addSubview:audioImgV];
    [backContainer addSubview:audioView];
    
    UIView *videoView=[[UIView alloc]initWithFrame:CGRectMake(0, backContainer.frame.size.height/2, FULLWIDTH, backContainer.frame.size.height/2)];
    UIButton *videoBTN = [[UIButton alloc]initWithFrame:CGRectMake(_mobileLBL.frame.origin.x, 0, FULLWIDTH-_mobileLBL.frame.origin.x, videoView.frame.size.height)];
    [videoBTN setTitle:@"Connex Video Call" forState:UIControlStateNormal];
    [videoBTN setTitleColor:TEXTCOLOR forState:UIControlStateNormal];
    videoBTN.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [videoBTN.titleLabel setFont:[UIFont systemFontOfSize:17.0f]];
    [videoBTN setTag:buttonTag];
    [videoView addSubview:videoBTN];
    UIView *divider1=[[UIView alloc]initWithFrame:CGRectMake(_mobileLBL.frame.origin.x, videoView.frame.size.height-1, FULLWIDTH-_mobileLBL.frame.origin.x, 1)];
    [divider1 setBackgroundColor:[UIColor lightGrayColor]];
    [divider1 setAlpha:0.5];
    [videoView addSubview:divider1];
    UIImageView *videoImgV = [[UIImageView alloc]initWithFrame:CGRectMake(audioView.frame.size.width - 50, (videoView.frame.size.height-32)/2, 33, 32)];
    [videoImgV setImage:[UIImage imageNamed:@"video-call"]];
    [videoView addSubview:videoImgV];
    [backContainer addSubview:videoView];
    
    [audioBTN addTarget:self action:@selector(voiceCall:) forControlEvents:UIControlEventTouchUpInside];
    [videoBTN addTarget:self action:@selector(videoCall:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return backContainer;
}

-(UIView *)createInviteViewAtY:(CGFloat)yPosition
{
    UIView *backContainer = [[UIView alloc]initWithFrame:CGRectMake(0, yPosition, FULLWIDTH, 50)];
    UIButton *inviteBTN = [[UIButton alloc]initWithFrame:CGRectMake(_mobileLBL.frame.origin.x, 5, FULLWIDTH-(_mobileLBL.frame.origin.x*2), backContainer.frame.size.height-10)];
    [inviteBTN setTitle:@"Invite to Connex" forState:UIControlStateNormal];
    [inviteBTN.titleLabel setFont:[UIFont systemFontOfSize:17.0f]];
    [inviteBTN setTitleColor:TEXTCOLOR forState:UIControlStateNormal];
    [inviteBTN setTag:buttonTag];
    [backContainer addSubview:inviteBTN];
    [inviteBTN addTarget:self action:@selector(messageToNumber:) forControlEvents:UIControlEventTouchUpInside];
    [self setRoundCornertoView:inviteBTN withBorderColor:UIColorFromRGB(0x007AFF) borderWidth:1.0f WithRadius:2 dependsOnHeight:NO];
    
    return backContainer;
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
}

- (IBAction)callToNumber:(UIButton *)sender
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Alert!"
                                          message:[NSString stringWithFormat:@"You are making a normal voice call, you will be charged accordingly and this call will not be encrypted as it is not a Connex call.Do you want to continue?"]
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                                   NSString *phone=[[userDic objectForKey:@"phone"] objectAtIndex:sender.tag];
                                   NSURL *phoneUrl;
                                   if ([phone length]>0)
                                       phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",[phone stringByReplacingOccurrencesOfString:@" " withString:@""]]];
                                   
                                   if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
                                   {
                                       [[UIApplication sharedApplication] openURL:phoneUrl];
                                   }
                                   else
                                   {
                                       UIAlertController *alert = [UIAlertController
                                                                             alertControllerWithTitle:@"Alert!"
                                                                             message:[NSString stringWithFormat:@"Oops Call facility is not available!!!"]
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                                       UIAlertAction *okAction = [UIAlertAction
                                                                      actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                                      style:UIAlertActionStyleCancel
                                                                      handler:^(UIAlertAction *action)
                                                                      {
                                                                      }];
                                       [alert addAction:okAction];
                                       
                                       [self presentViewController:alertController animated:YES completion:nil];
                                   }
                               }];
    UIAlertAction *cancelAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction *action)
                               {
                               }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


-(IBAction)messageToNumber:(UIButton *)sender
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    NSString *phone,*msgBody;
    if (![sender.titleLabel.text  isEqualToString: @"Invite to Connex"])
    {
        phone=[[userDic objectForKey:@"phone"] objectAtIndex:sender.tag];
        msgBody=@"";
    }
    else
    {
        phone=[phoneNumArray objectAtIndex:sender.tag];
        msgBody = @"Hey! Download Connex for secure voice and video calling! www.connex.tech/dw/";
    }
    NSString *phoneNumber = [NSString stringWithFormat:@"%@",[phone stringByReplacingOccurrencesOfString:@" " withString:@""]];
    if([MFMessageComposeViewController canSendText])
    {
        controller.recipients = [NSArray arrayWithObjects:phoneNumber, nil];
        controller.messageComposeDelegate = self;
        controller.body = msgBody;
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
    {
        case MessageComposeResultFailed:
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Alert!"
                                                  message:[NSString stringWithFormat:@"Oops failed to send message!!!"]
                                                  preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                       }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
        case MessageComposeResultSent:
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Success"
                                                  message:[NSString stringWithFormat:@"Message sent successfully"]
                                                  preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                       }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -- Favorite work here -- 
- (IBAction)addToFavorite:(id)sender
{
    @try
    {
        NSData* topImageData;
        if (profileImage != nil)
            topImageData  = UIImageJPEGRepresentation(_profilePic.image, 1.0);
        else
            topImageData = [NSData data];
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        [managedObjectContext performBlock:^{
            NSManagedObject *newObject = [NSEntityDescription insertNewObjectForEntityForName:@"FavoriteList" inManagedObjectContext:managedObjectContext];
            [newObject setValue:[userDic objectForKey:@"name"] forKey:@"name"];
            [newObject setValue:[userDic objectForKey:@"firstName"] forKey:@"firstName"];
            [newObject setValue:[userDic objectForKey:@"lastName"] forKey:@"lastName"];
            [newObject setValue:[userDic objectForKey:@"email"] forKey:@"email"];
            [newObject setValue:[userDic objectForKey:@"phone"] forKey:@"phone"];
            [newObject setValue:[userDic objectForKey:@"id"] forKey:@"id"];
            [newObject setValue:topImageData forKey:@"image"];
            [newObject setValue:[userDic objectForKey:@"postalAddress"] forKey:@"postalAddress"];
            [newObject setValue:connexUser forKey:@"connexUser"];
            NSError *error = nil;
            if ([managedObjectContext save:&error])
            {
                [_favoriteView setHidden:YES];
            }
        }];
    } @catch (NSException *exception)
    {   }
    @finally
    {   }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDel.callTimer invalidate];
        appDel.callTimer = nil;
    });
}

- (IBAction)backToPrevious:(id)sender
{
    if (_isFromLog)
        [self popToPreviousViewController:nil];
    else
        [self popToPreviousViewController:@"CContactListViewController"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
