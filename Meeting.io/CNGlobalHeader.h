//
//  CTGlobalHeader.h
//  Meeting.io
//
//  Created by Saptarshi's  on 9/24/15.
//  Copyright © 2015 Self. All rights reserved.
//


//URL DOMAIN

//#define DOMAINURL @"http://esolz.co.in/lab6/connex/jsonapp_control"
#define DOMAINURL @"http://ec2-52-34-171-68.us-west-2.compute.amazonaws.com/api/jsonapp_control"

#define APPBLUECOLOR [UIColor colorWithRed:0.0f/255.0f green:174.0f/255.0f blue:255.0f/255.0f alpha:1.0f]

#define TEXTCOLOR [UIColor colorWithRed:0.0f/255.0f green:122.0f/255.0f blue:255.0f/255.0f alpha:1.0f]

// Screen Size
#define IsIPad  [[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPad

#define  IsIphone5 (([[UIScreen mainScreen] bounds].size.height)==568)?true:false
#define  IsIphone6 (([[UIScreen mainScreen] bounds].size.height)==667)?true:false
#define  IsIphone6plus (([[UIScreen mainScreen] bounds].size.height)==736)?true:false
#define  IsIphone4 (([[UIScreen mainScreen] bounds].size.height)==480)?true:false

#define  FULLHEIGHT (float)[[UIScreen mainScreen] bounds].size.height
#define  FULLWIDTH  (float)[[UIScreen mainScreen] bounds].size.width

//Color
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//Font
#define AVENIR_BLACK @"AvenirLTStd-Black"
#define AVENIR_BOOK @"AvenirLTStd-Book"
#define AVENIR_LIGHT @"AvenirLTStd-Light"
#define AVENIR_MEDIUM @"AvenirLTStd-Medium"
#define AVENIR_ROMAN @"AvenirLTStd-Roman"

//app key file
#define CNUSERID @"user_id"
#define CTADMINID @"admin_id"
#define CNDEVICETOKEN @"deviceToken"
#define CNUSERNAME @"username"
#define CNPHONE @"phone_num"
#define CNEMAIL @"email_id"


#define PUSHMODE @"prod" // dev for dev ,prod for prod

#define IsSyncCompleted @"IsSyncCompleted"

//call status params
#define connexCallStatus @"connexCallStatus"

#define connexCallStatusRinging @"connexCallStatusRinging"
#define connexCallStatusOnCall @"connexCallStatusOnCall"
#define connexCallStatusCalling @"connexCallStatusCalling"
#define connexCallStatusIdle @"connexCallStatusIdle"

#define RINGINGTIME 15
#define CNBADGECOUNT @"badgeCount"

#ifdef DEBUG

#define DebugLog(...) NSLog(__VA_ARGS__)

#else

#define DebugLog(...) while(0)

#endif

/* MJGlobalHeader_h */
