//
//  CTGlobalViewController.h
//  Meeting.io
//
//  Created by Saptarshi's  on 9/24/15.
//  Copyright © 2015 Self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNGlobalHeader.h"
#import "MONActivityIndicatorView.h"
#import "SVProgressHUD.h"

#define HEADER_COLOR [UIColor colorWithRed:0.0f/255.0f green:174.0f/255.0f blue:255.0f/255.0f alpha:1.0f]

@interface CNGlobalViewController : UIViewController<NSURLSessionDelegate>
{
    MONActivityIndicatorView *indicatorView;
}

@property (retain,nonatomic)NSURLSessionDataTask *connectionSession;

-(CGFloat)setFontSize:(CGFloat)fontSize;
-(NSString *)trim:(NSString *)str;
-(void)textFieldErrorNotify:(UITextField *)textField;
-(BOOL) NSStringIsValidEmail:(NSString *)checkString;
-(void)setRoundCornertoView:(UIView *)viewname withBorderColor:(UIColor *)color borderWidth:(CGFloat)borderWidth WithRadius:(CGFloat)radius dependsOnHeight:(BOOL)dependsOnHeight;
-(void)setBorderToView:(UIView *)viewName withColor:(UIColor *)color withBorderWidth:(CGFloat)width;
- (CGSize)sizeOfTextView:(UITextView *)textView withText:(NSString *)text;
- (CGSize)sizeOfLabelwithFont:(UIFont *)font withText:(NSString *)text ofWidth:(CGFloat)width;
-(void)startLoadingOnView:(UIView *)view;
-(void)stopLoading;
//-(void)getJsonResponse :(NSString *)urlStr withPostData:(id)postParam paramName:(NSString *)paramName success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure;
-(void)PushViewController:(UIViewController *)viewController WithAnimation:(NSString *)AnimationType;
-(void)popToPreviousViewController:(id)className;

-(void)sendPushToUser:(NSString *)receiverID withRoomID:(NSString *)roomID withCallType:(NSString *)callType withStatus:(NSString *)status;

-(void)addTabBarToView:(UIView *)view;

- (NSManagedObjectContext *)managedObjectContext;
-(void)addToCallLogWithName:(NSString *)name phone:(NSString *)phone typeOfCall:(NSNumber *)calltype time:(NSDate *)time date:(NSString *)date fullDate:(NSDate *)dateString;
-(NSString *)checkStatusForMicrophoneAndCameraPermission;
-(void)showAlertForCameraAndMicrophonePermissions;
-(BOOL)isOnCall;
@end
