//
//  CNCallLogTableViewCell.h
//  Meeting.io
//
//  Created by Saptarshi's  on 7/13/16.
//  Copyright © 2016 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNCallLogTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *callTypeImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLBL;
@property (weak, nonatomic) IBOutlet UILabel *timeLBL;

@end
