//
//  CNLandingViewController.m
//  Meeting.io
//
//  Created by Saptarshi's  on 8/11/16.
//  Copyright © 2016 esolz. All rights reserved.
//

#import "CNLandingViewController.h"
#import "SMPageControl.h"
#import "CRegistrationViewController.h"
#import "CContactListViewController.h"
#import "KeychainItemWrapper.h"

static dispatch_once_t once;

@interface CNLandingViewController ()<UIScrollViewDelegate>
{
    NSMutableArray *imageArray,*textArray;
    AppDelegate *appDel;
    NSOperationQueue *opertaionQueue;
    
}
@property (weak, nonatomic) IBOutlet UIScrollView *imageScroll;
@property (weak, nonatomic) IBOutlet SMPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIView *splashView;

@end

@implementation CNLandingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    opertaionQueue = [[NSOperationQueue alloc]init];
    appDel=(AppDelegate *)[UIApplication sharedApplication].delegate;
    // Do any additional setup after loading the view.
    CGFloat fontSize,yPosition;
    fontSize = 12;
    if (IsIphone4)
        yPosition=40;
    else if (IsIphone5)
        yPosition=45;
    else if (IsIphone6)
        yPosition=50;
    else
        yPosition=55;
    
    NSDictionary *normalAttrDict = @{NSFontAttributeName : [UIFont systemFontOfSize:[self setFontSize:fontSize]]};
    NSDictionary *boldAttrDict = @{NSFontAttributeName : [UIFont boldSystemFontOfSize:[self setFontSize:fontSize]]};
    textArray = [NSMutableArray new];
    NSMutableAttributedString *textString;
    NSMutableAttributedString *normalText = [[NSMutableAttributedString alloc] initWithString:@"FOR ENHANCING THE EXPERIENCE WE\nNEED TO ACCESS YOUR" attributes: normalAttrDict];
    NSMutableAttributedString *boldText = [[NSMutableAttributedString alloc] initWithString:@" CONTACT\nLIST, CAMERA AND PHONE" attributes: boldAttrDict];
    textString = normalText;
    [textString appendAttributedString:boldText];
    [textArray addObject:textString];
    
    boldText = [[NSMutableAttributedString alloc] initWithString:@"FILTER" attributes: boldAttrDict];
    textString = boldText;
    normalText = [[NSMutableAttributedString alloc] initWithString:@" THROUGH" attributes: normalAttrDict];
    [textString appendAttributedString:normalText];
    boldText = [[NSMutableAttributedString alloc] initWithString:@" CONTACTS\n" attributes: boldAttrDict];
    [textString appendAttributedString:boldText];
    normalText = [[NSMutableAttributedString alloc] initWithString:@"SAME EXPERIENCE AS DEFAULT\nCONTACTS APP" attributes: normalAttrDict];
    [textString appendAttributedString:normalText];
    [textArray addObject:textString];
    
    boldText = [[NSMutableAttributedString alloc] initWithString:@"SEE IF THEY USE CONNEX\n" attributes: boldAttrDict];
    textString = boldText;
    normalText = [[NSMutableAttributedString alloc] initWithString:@"ELSE YOU CAN EASILY INVITE THEM\nTO JOIN" attributes: normalAttrDict];
    [textString appendAttributedString:normalText];
    [textArray addObject:textString];
    
    boldText = [[NSMutableAttributedString alloc] initWithString:@"SECURE VOICE AND VIDEO CALLS\n" attributes: boldAttrDict];
    textString = boldText;
    normalText = [[NSMutableAttributedString alloc] initWithString:@"WITH ALL STANDARD CALLING FEATURES LIKE\nMUTE, SPEAKER ETC" attributes: normalAttrDict];
    [textString appendAttributedString:normalText];
    [textArray addObject:textString];
    [textArray addObject:textString];
    imageArray=[NSMutableArray arrayWithObjects:@"tutorial1.png",@"tutorial2.png",@"tutorial3.png",@"tutorial4.png",@"tutorial5", nil];
    
    _pageControl.numberOfPages=imageArray.count;
    _pageControl.indicatorMargin = 5.0f;
    _pageControl.indicatorDiameter = .5f;
    _pageControl.currentPage=0;
    _pageControl.minHeight=2.0f;
    [_pageControl setPageIndicatorImage:[UIImage imageNamed:@"slider_dot_white"]];
    [_pageControl setCurrentPageIndicatorImage:[UIImage imageNamed:@"slider_dot_fill_white"]];
    
    for (int i=0; i<imageArray.count; i++)
    {
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(i*FULLWIDTH, 0, FULLWIDTH, FULLHEIGHT)];
        [_imageScroll addSubview:img];
        [img setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[imageArray objectAtIndex:i]]]];
        
        UILabel *textLBL=[[UILabel alloc]init];
        
        [textLBL setTextColor:[UIColor whiteColor]];
        [textLBL setAttributedText:[textArray objectAtIndex:i]];
        [textLBL setTextAlignment:NSTextAlignmentCenter];
        [textLBL setNumberOfLines:0];
//        [textLBL setFont:[UIFont systemFontOfSize:15]];
        [_imageScroll addSubview:textLBL];
        [textLBL sizeToFit];
        CGFloat lblWidth=textLBL.frame.size.width;
        [textLBL setFrame:CGRectMake((FULLWIDTH-lblWidth)/2+i*FULLWIDTH, yPosition, lblWidth, textLBL.frame.size.height)];
    }
    
    [_imageScroll setContentSize:CGSizeMake(FULLWIDTH*imageArray.count, 0)];
    
    NSString *userID=[[NSUserDefaults standardUserDefaults]objectForKey:CNUSERID];
    NSString *name=[[NSUserDefaults standardUserDefaults]objectForKey:CNUSERNAME];
    NSString *phone=[[NSUserDefaults standardUserDefaults]objectForKey:CNPHONE];
    NSString *email=[[NSUserDefaults standardUserDefaults]objectForKey:CNEMAIL];
//    KeychainItemWrapper *userIDKey = [[KeychainItemWrapper alloc] initWithIdentifier:CNUSERID accessGroup:nil];
//    NSString *userID = [userIDKey objectForKey:(__bridge id)(kSecAttrAccount)];
//    KeychainItemWrapper *emailKey = [[KeychainItemWrapper alloc] initWithIdentifier:CNEMAIL accessGroup:nil];
//    NSString *email = [emailKey objectForKey:(__bridge id)(kSecAttrAccount)];
//    KeychainItemWrapper *userNameKey = [[KeychainItemWrapper alloc] initWithIdentifier:CNPHONE accessGroup:nil];
//    NSString *phone = [userNameKey objectForKey:(__bridge id)(kSecAttrAccount)];
//    KeychainItemWrapper *nameKey = [[KeychainItemWrapper alloc] initWithIdentifier:CNUSERNAME accessGroup:nil];
//    NSString *name = [nameKey objectForKey:(__bridge id)(kSecAttrAccount)];
    
    
    if ([userID length]>0 && phone!=nil)
    {
        [appDel fetchContact];
        [_splashView setHidden:NO];
        [self startLoadingOnView:self.view];
        [opertaionQueue addOperationWithBlock:^{
            [self didRegisterWithNumber:phone withName:name withEmail:email];
        }];
    }
    else
    {
        [_splashView setHidden:YES];
        //        [_nameTF setText:@"saptarshi"];
        //        [_emailTF setText:@"saptarshi.goswami@esolzmail.com"];
        //        [_phoneTF setText:@"9876543210"];
        //        [_nameTF setText:@"bhaswar"];
        //        [_emailTF setText:@"bhaswar.mukherjee@esolzmail.com"];
        //        [_phoneTF setText:@"9007853386"];
    }
}

-(void)didRegisterWithNumber:(NSString *)phone withName:(NSString *)name withEmail:(NSString *)email
{
    NSArray *keys = [NSArray arrayWithObjects:@"name", @"phone", @"email",@"device_token_ios",@"device_token_android", nil];
    
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults]objectForKey:CNDEVICETOKEN];
    if (deviceToken == nil || [deviceToken isKindOfClass:[NSNull class]])
    {
        deviceToken=@"";
    }
    
    NSArray *objects = [NSArray arrayWithObjects:[name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]], [phone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]], [email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],deviceToken,@"", nil];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    NSMutableArray *finalDict = [[NSMutableArray alloc]init];
    
    //    [finalDict setObject:dictionary forKey:@"details_info"];
    [finalDict addObject:dictionary];
    
    NSError *localErr;
    
    NSHTTPURLResponse *response = nil;
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:finalDict options:NSJSONWritingPrettyPrinted error:&localErr];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    
    DebugLog(@"json string ertta: %@",jsonString);
    
    NSMutableString *urlString=[NSMutableString stringWithFormat:@"%@/login?details_info=%@",DOMAINURL,[self encodeToPercentEscapeString:jsonString]];
    
    NSLog(@"url string %@",urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    //-- Get request and response though URL
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //-- JSON Parsing
    NSMutableDictionary *result;
    if (responseData != nil)
        result = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_splashView setHidden:YES];
            [self stopLoading];
        });
    }
    NSLog(@"Result = %@",result);
    if (result != nil)
    {
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            if ([[result objectForKey:@"response"]isEqualToString:@"TRUE"])
            {
                [self stopLoading];
                
//                KeychainItemWrapper *userIDKey = [[KeychainItemWrapper alloc] initWithIdentifier:CNUSERID accessGroup:nil];
//                [userIDKey setObject:[NSString stringWithFormat:@"%@",[result valueForKey:@"user_id"]] forKey:(__bridge id)(kSecAttrAccount)];
//                KeychainItemWrapper *emailKey = [[KeychainItemWrapper alloc] initWithIdentifier:CNEMAIL accessGroup:nil];
//                [emailKey setObject:email forKey:(__bridge id)(kSecAttrAccount)];
//                KeychainItemWrapper *userNameKey = [[KeychainItemWrapper alloc] initWithIdentifier:CNPHONE accessGroup:nil];
//                [userNameKey setObject:[NSString stringWithFormat:@"%@",phone] forKey:(__bridge id)(kSecAttrAccount)];
//                KeychainItemWrapper *nameKey = [[KeychainItemWrapper alloc] initWithIdentifier:CNUSERNAME accessGroup:nil];
//                [nameKey setObject:name forKey:(__bridge id)(kSecAttrAccount)];
                [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",[result valueForKey:@"user_id"]] forKey:CNUSERID];
                [[NSUserDefaults standardUserDefaults]setObject:email forKey:CNEMAIL];
                [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",phone] forKey:CNPHONE];
                [[NSUserDefaults standardUserDefaults]setObject:name forKey:CNUSERNAME];
                CContactListViewController *CCDVC=[[UIStoryboard storyboardWithName:@"IPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"CContactListViewController"];
                [self PushViewController:CCDVC WithAnimation:kCAMediaTimingFunctionEaseIn];
            }
            else
            {
                [self stopLoading];
                [_splashView setHidden:YES];
            }
        }];
    }
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string
{
    NSLog(@"encode");
    return [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    CGFloat pageWidth = _imageScroll.frame.size.width;
    int page = floor((self.imageScroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = _imageScroll.frame.size.width;
    int page = floor((self.imageScroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (page == 1)
    {
        dispatch_once(&once, ^{
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                
            }];
            
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
                
            }];
            
            [appDel fetchContact];
        });
        
    }
}

- (IBAction)skipTutorial:(id)sender
{
    [self getPermissionsAndRedirectToregistration];
}

- (IBAction)nextTutorial:(id)sender
{
    CGFloat pageWidth = _imageScroll.frame.size.width;
    int page = floor((self.imageScroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (page < [imageArray count]-1)
    {
        CGFloat xpoint=(page+1)*FULLWIDTH;
        [_imageScroll setContentOffset:CGPointMake(xpoint, 0) animated:YES];
        if (page == 0)
        {
            dispatch_once(&once, ^{
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    
                }];
                
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
                    
                }];
                
                [appDel fetchContact];
            });
            
        }
    }
    else
    {
        [self getPermissionsAndRedirectToregistration];
    }
}

-(void)getPermissionsAndRedirectToregistration
{
    CRegistrationViewController *CRVC=[[UIStoryboard storyboardWithName:@"IPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"CRegistrationViewController"];
    [self PushViewController:CRVC WithAnimation:kCAMediaTimingFunctionEaseIn];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
