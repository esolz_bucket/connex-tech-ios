//
//  CNContactTableViewCell.h
//  Connex-New
//
//  Created by Saptarshi's  on 6/24/16.
//  Copyright © 2016 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNContactTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *contactImageV;
@property (weak, nonatomic) IBOutlet UILabel *contactName;
@property (weak, nonatomic) IBOutlet UIImageView *connexUserImgV;

@end
