//
//  CTCustomNavigationController.m
//  Meeting.io
//
//  Created by Saptarshi's  on 5/31/16.
//  Copyright © 2016 esolz. All rights reserved.
//

#import "CNCustomNavigationController.h"

@interface CNCustomNavigationController ()

@end

@implementation CNCustomNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate
{
    if (!(IsIPad))
    {
        return NO;
    }
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if (!(IsIPad))
    {
        return UIInterfaceOrientationMaskPortrait;
    }
    return UIInterfaceOrientationMaskLandscapeLeft;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

@end
