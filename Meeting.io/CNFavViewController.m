//
//  CNFavViewController.m
//  AppRTC
//
//  Created by Saptarshi's  on 7/7/16.
//  Copyright © 2016 ISBX. All rights reserved.
//

#import "CNFavViewController.h"
#import "CNContactTableViewCell.h"
#import "CContactDetailsViewController.h"
#import "AppDelegate.h"

@interface CNFavViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *userArray;
    NSIndexPath *indexpathSelected;
}
@property (weak, nonatomic) IBOutlet UITableView *contactListTable;
@property (weak, nonatomic) IBOutlet UILabel *errorLBL;

@end

@implementation CNFavViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTabBarToView:self.view];
    [_contactListTable setHidden:YES];
    [_errorLBL setHidden:YES];
    userArray=[NSMutableArray new];
    [self fetchFavoriteList];
}

-(void)fetchFavoriteList
{
    @try {
        
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        
        [managedObjectContext performBlock:^{
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"FavoriteList"];
            NSError *error = nil;
            NSUInteger count = [managedObjectContext countForFetchRequest:fetchRequest
                                                                    error:&error];
            if (count>0)
            {
                NSMutableArray *list = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                NSLog(@"number of list : %d",(int)list.count);
                for (NSManagedObject *user in list)
                {
                    NSMutableDictionary *mutDic=[NSMutableDictionary new];
                    [mutDic setObject:[NSString stringWithFormat:@"%@",[user valueForKey:@"name"]] forKey:@"name"];
                    [mutDic setObject:[NSString stringWithFormat:@"%@",[user valueForKey:@"firstName"]] forKey:@"firstName"];
                    [mutDic setObject:[NSString stringWithFormat:@"%@",[user valueForKey:@"lastName"]] forKey:@"lastName"];
                    
                    [mutDic setObject:(NSArray *)[user valueForKey:@"phone"] forKey:@"phone"];
                    if ([user valueForKey:@"image"] != nil)
                        [mutDic setObject:[user valueForKey:@"image"] forKey:@"image"];
                    else
                        [mutDic setObject:@"" forKey:@"image"];
                    [mutDic setObject:(NSArray *)[user valueForKey:@"email"] forKey:@"email"];
                    [mutDic setObject:[user valueForKey:@"id"] forKey:@"id"];
                    [mutDic setObject:[user valueForKey:@"connexUser"] forKey:@"connexUser"];
                    [mutDic setObject:[user valueForKey:@"postalAddress"] forKey:@"postalAddress"];
                    [userArray addObject:mutDic];
                }
                
                [_contactListTable reloadData];
                [_contactListTable setHidden:NO];
                [self stopLoading];
            }
            else
            {
                [_contactListTable setHidden:YES];
                [_errorLBL setHidden:NO];
            }
        }];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableVIewDataSource and UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return userArray.count;
}

- (CNContactTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CNContactTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"contactCell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(CNContactTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *contact;
    contact = [userArray objectAtIndex:indexPath.row];
    
    if ([[contact objectForKey:@"connexUser"]isEqualToString:@"YES"])
        [cell.connexUserImgV setHidden:NO];
    else
        [cell.connexUserImgV setHidden:YES];
    [cell.contactName setText:[NSString stringWithFormat:@"%@",contact[@"name"]]];
    [self setRoundCornertoView:cell.connexUserImgV withBorderColor:nil borderWidth:0.0f WithRadius:.5 dependsOnHeight:YES];
    
    NSDictionary *font_regular = [[NSDictionary alloc]init];
    NSDictionary *font_bold = [[NSDictionary alloc]init];
    font_regular = @{NSFontAttributeName: [UIFont systemFontOfSize:17],
                     
                     NSForegroundColorAttributeName: [UIColor blackColor]};
    
    font_bold = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17],
                  
                  NSForegroundColorAttributeName: [UIColor blackColor]};
    
    NSMutableAttributedString *attrString1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",contact[@"firstName"]] attributes: font_regular];
    
    NSMutableAttributedString *attrString2= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",contact[@"lastName"]] attributes: font_bold];
    
    [attrString1 appendAttributedString:attrString2];
    [cell.contactName setAttributedText:attrString1];
    UIImage *proImage;
    id imageType = [contact objectForKey:@"image"];
    if ([imageType isKindOfClass:[NSData class]])
        proImage=[UIImage imageWithData:[contact objectForKey:@"image"]];
    [self setRoundCornertoView:cell.contactImageV withBorderColor:nil borderWidth:0 WithRadius:0.5 dependsOnHeight:YES];
    if (proImage !=nil)
        [cell.contactImageV setImage:proImage];
    else
    {
        NSMutableString *initisals=[[NSMutableString alloc]init];
        if ([cell.contactName.text length] > 0) {
            NSString * firstLetter = [cell.contactName.text substringWithRange:[cell.contactName.text rangeOfComposedCharacterSequenceAtIndex:0]];
            [initisals appendString:[firstLetter uppercaseString]];
        }
        UILabel *nameLabel=[[UILabel alloc]initWithFrame:cell.contactImageV.frame];
        [nameLabel setBackgroundColor:[UIColor lightGrayColor]];
        [nameLabel setTextColor:[UIColor whiteColor]];
        [nameLabel setFont:[UIFont systemFontOfSize:16]];
        [nameLabel setTextAlignment:NSTextAlignmentCenter];
        [cell.contentView addSubview:nameLabel];
        [nameLabel setText:initisals];
        [self setRoundCornertoView:nameLabel withBorderColor:nil borderWidth:0 WithRadius:.5 dependsOnHeight:YES];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
    //    [searchBar resignFirstResponder];
    indexpathSelected = indexPath;
    NSLog(@"index path row %ld section %ld",indexpathSelected.row,indexpathSelected.section);
    CContactDetailsViewController *CCDVC=[[UIStoryboard storyboardWithName:@"IPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"CContactDetailsViewController"];
    CCDVC.userPhoneNumber = [[[userArray objectAtIndex:indexPath.row] valueForKey:@"phone"] objectAtIndex:0];
    [self PushViewController:CCDVC WithAnimation:kCAMediaTimingFunctionEaseIn];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        NSString *contactId=[[userArray objectAtIndex:indexPath.row] objectForKey:@"id"];
        NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"FavoriteList"];
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"id==%@",contactId]; // If required to fetch specific user
        fetchRequest.predicate=predicate;
        NSError *error=nil;
        NSManagedObject *user=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil]lastObject];
        if (user!= nil)
        {
            [managedObjectContext deleteObject:user];
            [managedObjectContext save:&error];
            [userArray removeObjectAtIndex:indexPath.row];
            [tableView beginUpdates];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            [tableView endUpdates];
            if ([userArray count]==0)
            {
                [_contactListTable setHidden:YES];
                [_errorLBL setHidden:NO];
            }
        }
    }
}

- (IBAction)backToPrevious:(id)sender
{
    [self popToPreviousViewController:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    if (indexpathSelected != nil && indexpathSelected.row != NSNotFound && indexpathSelected.section != NSNotFound)
    {
        [[_contactListTable cellForRowAtIndexPath:indexpathSelected] setBackgroundColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:1.0f]];
        [_contactListTable reloadRowsAtIndexPaths:@[indexpathSelected] withRowAnimation:UITableViewRowAnimationNone];
    }
}

@end
