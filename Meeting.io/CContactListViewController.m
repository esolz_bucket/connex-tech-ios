//
//  CNContactListViewController.m
//  Connex-New
//
//  Created by Saptarshi's  on 6/24/16.
//  Copyright © 2016 esolz. All rights reserved.
//

#import "CContactListViewController.h"
#import <Contacts/Contacts.h>
#import "CNContactTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "CContactDetailsViewController.h"
#import "AppDelegate.h"
#import "ARTCVideoChatViewController.h"
#import "CNFavViewController.h"
#import <AddressBook/AddressBook.h>

@interface CContactListViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UIScrollViewDelegate>
{
    NSMutableArray *contactArray,*filteredArray;
    NSMutableDictionary *indexedDic;
    BOOL isFiltered,isAnimating;
    UIView *refreshView;
    UIRefreshControl *refreshControl;
    NSMutableArray *lblArray;
    NSTimer *timer;
    int currentColorIndex,currentLabelIndex;
    NSOperationQueue *opertaionQueue;
    NSIndexPath *indexpath_selected;
    AppDelegate *appDel;
}
@property (weak, nonatomic) IBOutlet UILabel *headerLBL;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *contactListTable;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@end

@implementation CContactListViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    appDel=(AppDelegate *)[UIApplication sharedApplication].delegate;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(fetchContacts) name:@"ContactsLoaded" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(ContactStoreUpdated:) name:CNContactStoreDidChangeNotification object:nil];
}

-(void)ContactStoreUpdated:(NSNotification *)noti
{
    NSLog(@"contact store updated %@",noti.userInfo);
    if (!appDel.isUpdateNotificationReceived)
    {
        [appDel fetchContact];
        appDel.isUpdateNotificationReceived=YES;
    }
}

-(void)syncContacts
{
    [appDel fetchContact];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    indexedDic=[NSMutableDictionary new];
    filteredArray=[NSMutableArray new];
    contactArray = [NSMutableArray new];
    opertaionQueue =[[NSOperationQueue alloc]init];
    currentColorIndex = currentLabelIndex = 0;
    lblArray=[NSMutableArray new];
    refreshControl = [[UIRefreshControl alloc]init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self action:@selector(syncContacts) forControlEvents:UIControlEventValueChanged];
    [_contactListTable addSubview:refreshControl];
    
    isFiltered=NO;
    [_searchBar setFrame:CGRectMake(_searchBar.frame.origin.x, _headerView.frame.origin.y+_headerView.frame.size.height, FULLWIDTH, _searchBar.frame.size.height)];
    [_contactListTable setFrame:CGRectMake(_contactListTable.frame.origin.x, _searchBar.frame.origin.y+_searchBar.frame.size.height, FULLWIDTH, FULLHEIGHT-(_searchBar.frame.origin.y+_searchBar.frame.size.height)-49)];
    [self startLoadingOnView:self.view];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self addTabBarToView:self.view];
    NSString *status = [[NSUserDefaults standardUserDefaults]objectForKey:IsSyncCompleted];
    if ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusAuthorized)
    {
        if ([status isEqualToString:@"SyncCompleted"])
        {
            [self fetchContacts];
        }
    }
    else
    {
        [self stopLoading];
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Error!!!"
                                              message:[NSString stringWithFormat:@"You must enable contact permission to fetch all your contacts."]
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                   }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}
-(void)fetchContacts
{
    if ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts] == CNAuthorizationStatusAuthorized)
    {
        @try
        {
            NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
            
            [managedObjectContext performBlock:^{
                
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"ContactList"];
                NSError *error = nil;
                NSUInteger count = [managedObjectContext countForFetchRequest:fetchRequest
                                                                        error:&error];
                if (count>0)
                {
                    NSMutableArray *list = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                    NSLog(@"number of list : %d",(int)list.count);
                    NSString *status = [[NSUserDefaults standardUserDefaults]objectForKey:IsSyncCompleted];
                    if ([status isEqualToString:@"SyncCompleted"])
                    {
                        contactArray = [NSMutableArray new];
                        for (NSManagedObject *user in list)
                        {
                            NSMutableDictionary *mutDic=[NSMutableDictionary new];
                            [mutDic setObject:[NSString stringWithFormat:@"%@",[user valueForKey:@"name"]] forKey:@"name"];
                            [mutDic setObject:[NSString stringWithFormat:@"%@",[user valueForKey:@"firstName"]] forKey:@"firstName"];
                            [mutDic setObject:[NSString stringWithFormat:@"%@",[user valueForKey:@"lastName"]] forKey:@"lastName"];
                            
                            [mutDic setObject:(NSArray *)[user valueForKey:@"phone"] forKey:@"phone"];
                            [mutDic setObject:[NSString stringWithFormat:@"%@",[user valueForKey:@"image"]] forKey:@"image"];
                            [mutDic setObject:(NSArray *)[user valueForKey:@"email"] forKey:@"email"];
                            [mutDic setObject:[user valueForKey:@"id"] forKey:@"id"];
                            [mutDic setObject:[user valueForKey:@"connexUser"] forKey:@"connexUser"];
                            
                            [mutDic setObject:(NSArray *)[user valueForKey:@"connexID"] forKey:@"connexID"];
                            NSMutableArray *postalAddress=[NSMutableArray new];
                            if ([user valueForKey:@"postalAddress"] != nil)
                                postalAddress = [[user valueForKey:@"postalAddress"] mutableCopy];
                            [mutDic setObject:postalAddress forKey:@"postalAddress"];
                            [contactArray addObject:mutDic];
                        }
                        
                        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
                        NSArray *sortedArray=[contactArray sortedArrayUsingDescriptors:@[sort]];
                        contactArray = [sortedArray mutableCopy];
                        indexedDic = [[self sortList:contactArray] mutableCopy];
                        [refreshControl endRefreshing];
                        [_contactListTable reloadData];
                        [_contactListTable setHidden:NO];
                        [self stopLoading];
                    }
                }
                else
                {
                    [self stopLoading];
                }
            }];
        }
        @catch (NSException *exception) {   }
        @finally{    }
    }
    else
    {
        [self stopLoading];
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Error!!!"
                                              message:[NSString stringWithFormat:@"You must enable contact permission to fetch all your contacts."]
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                   }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isFiltered)
        return [filteredArray count];
    else
        return [[indexedDic objectForKey:[NSNumber numberWithInteger:section]] count];
}

- (CNContactTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CNContactTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"contactCell" forIndexPath:indexPath];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(CNContactTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *contact;
    if (isFiltered)
        contact = [filteredArray objectAtIndex:indexPath.row];
    else
        contact=[[indexedDic objectForKey:[NSNumber numberWithInteger:indexPath.section]] objectAtIndex:indexPath.row];;
    if ([[contact objectForKey:@"connexUser"]isEqualToString:@"YES"])
        [cell.connexUserImgV setHidden:NO];
    else
        [cell.connexUserImgV setHidden:YES];
    [cell.contactName setText:[NSString stringWithFormat:@"%@",contact[@"name"]]];
    [self setRoundCornertoView:cell.connexUserImgV withBorderColor:nil borderWidth:0.0f WithRadius:.5 dependsOnHeight:YES];
    
    NSDictionary *font_regular = [[NSDictionary alloc]init];
    NSDictionary *font_bold = [[NSDictionary alloc]init];
    font_regular = @{NSFontAttributeName: [UIFont systemFontOfSize:17],
                     
                     NSForegroundColorAttributeName: [UIColor blackColor]};
    
    font_bold = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:17],
                     
                     NSForegroundColorAttributeName: [UIColor blackColor]};

    NSMutableAttributedString *attrString1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",contact[@"firstName"]] attributes: font_regular];
    
    NSMutableAttributedString *attrString2= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",contact[@"lastName"]] attributes: font_bold];
    
    [attrString1 appendAttributedString:attrString2];
    [cell.contactName setAttributedText:attrString1];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0f;
}

#pragma mark TableView Section Indexing Delegate Methods  /////27/8/2015

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (!isFiltered)
    {
        UILocalizedIndexedCollation *currentCollation = [UILocalizedIndexedCollation currentCollation];
        return [currentCollation sectionForSectionIndexTitleAtIndex:index];
    }
    else
        return 0;
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (!isFiltered)
        return [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles];
    else
        return Nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (!isFiltered)
    {
        return [[[UILocalizedIndexedCollation currentCollation] sectionTitles] count];
    }
    else
        return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (isFiltered)
        return 0;
    else
    {
        BOOL showSection = [[indexedDic objectForKey:[NSNumber numberWithInteger:section]] count]!=0;
        if (showSection)
            return  [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section];
        else
            return 0;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (isFiltered)
        return 0;
    else
    {
        BOOL showSection = [[indexedDic objectForKey:[NSNumber numberWithInteger:section]] count]!=0;
        if (showSection)
            return  30.0f;
        else
            return 0;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:[UIColor colorWithRed:(239.0f/255.0f) green:(239.0f/255.0f) blue:(239.0f/255.0f) alpha:1.0f]];
    indexpath_selected = indexPath;
    NSLog(@"index path row %ld section %ld",indexpath_selected.row,indexpath_selected.section);
    CContactDetailsViewController *CCDVC=[[UIStoryboard storyboardWithName:@"IPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"CContactDetailsViewController"];
    if (isFiltered)
        CCDVC.userPhoneNumber = [[[filteredArray objectAtIndex:indexPath.row] valueForKey:@"phone"] objectAtIndex:0];
    else
        CCDVC.userPhoneNumber=[[[[indexedDic objectForKey:[NSNumber numberWithInteger:indexPath.section]] objectAtIndex:indexPath.row] valueForKey:@"phone"] objectAtIndex:0];
    appDel.userPhoneNumber=CCDVC.userPhoneNumber;
    NSLog(@"appDel.userDic %@",appDel.userPhoneNumber);
    [self PushViewController:CCDVC WithAnimation:kCAMediaTimingFunctionEaseIn];
}

-(NSDictionary *)sortList:(NSArray *)userArray
{
    NSDictionary *listDic=[NSDictionary dictionary];
    NSMutableDictionary *tempSections = [NSMutableDictionary dictionary];
    
    // iterate through each dictionaey in the list, and put them into the correct section
    for (NSDictionary *item in userArray)
    {
        // get the index of the section (Assuming the table index is showing A-#)
        NSInteger indexName = [[UILocalizedIndexedCollation currentCollation] sectionForObject:[item valueForKey:@"name"] collationStringSelector:@selector(description)];
        
        NSNumber *keyName = [NSNumber numberWithInteger:indexName];
        
        // if an array doesnt exist for the key, create one
        NSMutableArray *arrayName = [tempSections objectForKey:keyName];
        if (arrayName == nil)
        {
            arrayName = [NSMutableArray array];
        }
        
        // add the dictionary to the array (add the actual value as we need this object to sort the array later)
        [arrayName addObject: item];   //[item valueForKey:@"fullname"]];
        
        // put the array with new object in, back into the dictionary for the correct key
        [tempSections setObject:arrayName forKey:keyName];
    }
    
    /* now to do the sorting of each index */
    
    NSMutableDictionary *sortedSections = [NSMutableDictionary dictionary];
    
    // sort each index array (A..Z)
    [tempSections enumerateKeysAndObjectsUsingBlock:^(id key, id array, BOOL *stop)
     {
         // sort the array - again, we need to tell it which selctor to sort each object by
         NSArray *sortedArray = [[UILocalizedIndexedCollation currentCollation] sortedArrayFromArray:array collationStringSelector:@selector(description)];
         
         NSSortDescriptor *sort =[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
         sortedArray=[[sortedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]] mutableCopy];
         [sortedSections setObject:[NSMutableArray arrayWithArray:sortedArray] forKey:key];
     }];
    
    // set the global sectioned dictionary
    listDic = sortedSections;
    return listDic;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    isFiltered=YES;
    if ([searchText length]>0)
        [self searchContact:[searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    else
    {
//        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
//        NSArray *sortedArray=[contactArray sortedArrayUsingDescriptors:@[sort]];
        filteredArray = [contactArray mutableCopy];
        
        [_contactListTable reloadData];
    }
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if ([searchBar.text length]>0)
        [self searchContact:[searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    else
    {
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        NSArray *sortedArray=[contactArray sortedArrayUsingDescriptors:@[sort]];
        filteredArray = [sortedArray mutableCopy];
        [_contactListTable reloadData];
    }
    [_searchBar resignFirstResponder];
}

-(void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    isFiltered=NO;
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar setText:nil];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSArray *sortedArray=[contactArray sortedArrayUsingDescriptors:@[sort]];
    contactArray = [sortedArray mutableCopy];
    indexedDic = [[self sortList:contactArray] mutableCopy];
    
    [_contactListTable reloadData];
}

-(void)searchContact:(NSString *)searchString
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"name contains[c] %@",searchString];
    NSMutableArray *searchresults = [NSMutableArray arrayWithArray:[contactArray filteredArrayUsingPredicate:resultPredicate]];
    
    filteredArray=[[NSMutableArray alloc] initWithArray:searchresults];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    NSArray *sortedArray=[filteredArray sortedArrayUsingDescriptors:@[sort]];
    filteredArray = [sortedArray mutableCopy];
//    indexedDic = [[self sortList:filteredArray] mutableCopy];
    
    [_contactListTable reloadData];
}

-(void)loadRefreshControl
{
    refreshView = [[[NSBundle mainBundle]loadNibNamed:@"RefreshContents" owner:self options:nil]objectAtIndex:0];
    refreshView.frame = refreshControl.bounds;
    for( int i=0; i < refreshView.subviews.count; ++i)
    {
        UILabel *lbl=(UILabel *)[refreshView viewWithTag:i+1];
        [lblArray addObject:lbl];
    }
    
    [refreshControl addSubview:refreshView];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (!isAnimating)
    {
        //[self doSomething];
        //[self animateRefreshStep1];
    }
}

-(void)animateRefreshStep1
{
    isAnimating=YES;
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        UILabel *lbl=lblArray[currentLabelIndex];
        [lbl setTransform:CGAffineTransformMakeRotation(M_PI_4)];
        [lbl setTextColor:[self getNextColor]];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.05 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            UILabel *lbl=lblArray[currentLabelIndex];
            [lbl setTransform:CGAffineTransformIdentity];
            [lbl setTextColor:[UIColor whiteColor]];
        } completion:^(BOOL finished) {
            ++currentLabelIndex;
            if (currentLabelIndex < lblArray.count)
            {
                [self animateRefreshStep1];
            }
            else
            {
                [self animateRefreshStep2];
            }
        }];
    }];
}

-(void)animateRefreshStep2
{
    [UIView animateWithDuration:0.35 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        UILabel *lbl;
        lbl = lblArray[0];
        lbl.transform = CGAffineTransformMakeScale(1.5, 1.5);
        lbl = lblArray[1];
        lbl.transform = CGAffineTransformMakeScale(1.5, 1.5);
        lbl = lblArray[2];
        lbl.transform = CGAffineTransformMakeScale(1.5, 1.5);
        lbl = lblArray[3];
        lbl.transform = CGAffineTransformMakeScale(1.5, 1.5);
        lbl = lblArray[4];
        lbl.transform = CGAffineTransformMakeScale(1.5, 1.5);
        lbl = lblArray[5];
        lbl.transform = CGAffineTransformMakeScale(1.5, 1.5);
        lbl = lblArray[6];
        lbl.transform = CGAffineTransformMakeScale(1.5, 1.5);
        lbl = lblArray[7];
        lbl.transform = CGAffineTransformMakeScale(1.5, 1.5);
        lbl = lblArray[8];
        lbl.transform = CGAffineTransformMakeScale(1.5, 1.5);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            UILabel *lbl;
            lbl = lblArray[0];
            lbl.transform = CGAffineTransformIdentity;
            lbl = lblArray[1];
            lbl.transform = CGAffineTransformIdentity;
            lbl = lblArray[2];
            lbl.transform = CGAffineTransformIdentity;
            lbl = lblArray[3];
            lbl.transform = CGAffineTransformIdentity;
            lbl = lblArray[4];
            lbl.transform = CGAffineTransformIdentity;
            lbl = lblArray[5];
            lbl.transform = CGAffineTransformIdentity;
            lbl = lblArray[6];
            lbl.transform = CGAffineTransformIdentity;
            lbl = lblArray[7];
            lbl.transform = CGAffineTransformIdentity;
            lbl = lblArray[8];
            lbl.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            if ([refreshControl isRefreshing])
            {
                currentLabelIndex = 0;
                [self animateRefreshStep1];
            }
            else
            {
                isAnimating=NO;
                currentLabelIndex = 0;
                for (int i=0 ; i<lblArray.count ; ++i)
                {
                    UILabel *lbl = lblArray[i];
                    [lbl setTextColor:[UIColor whiteColor]];
                    [lbl setTransform:CGAffineTransformIdentity];
                }
            }
        }];
    }];
}

-(UIColor *)getNextColor
{
    NSArray *colorArray = [NSArray arrayWithObjects:[UIColor magentaColor],[UIColor brownColor],[UIColor yellowColor],[UIColor redColor],[UIColor greenColor],[UIColor blueColor],[UIColor orangeColor], nil];
    if (currentColorIndex == colorArray.count)
        currentColorIndex= 0;
    
    UIColor *returnColor=colorArray[currentColorIndex];
    ++currentColorIndex;
    return returnColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated
{
    
    [super viewDidDisappear:animated];
    appDel.isUpdateNotificationReceived=NO;
    [[NSNotificationCenter defaultCenter]removeObserver:self];
//    NSInteger row = indexpath_selected.row;
//    NSInteger section = indexpath_selected.section;
    if (indexpath_selected != nil && indexpath_selected.row != NSNotFound && indexpath_selected.section != NSNotFound)
    {
        [[_contactListTable cellForRowAtIndexPath:indexpath_selected] setBackgroundColor:[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(255.0f/255.0f) alpha:1.0f]];
        [_contactListTable reloadRowsAtIndexPaths:@[indexpath_selected] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark -- Favorite work here --

- (IBAction)favoriteList:(id)sender
{
    CNFavViewController *CNFVC=[[UIStoryboard storyboardWithName:@"IPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"CNFavViewController"];
    [self PushViewController:CNFVC WithAnimation:kCAMediaTimingFunctionEaseIn];
}

@end
