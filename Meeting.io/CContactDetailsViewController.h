//
//  CContactDetailsViewController.h
//  Connex-New
//
//  Created by Saptarshi's  on 6/25/16.
//  Copyright © 2016 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNGlobalViewController.h"

@interface CContactDetailsViewController : CNGlobalViewController

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *nameLBL;
//@property (weak, nonatomic) IBOutlet UILabel *firstPhoneLBL;
@property (assign, nonatomic)BOOL isFromLog;
@property (retain,nonatomic) NSString *userPhoneNumber;

@end
