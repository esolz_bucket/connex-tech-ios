//
//  CTGlobalViewController.m
//  Meeting.io
//
//  Created by Saptarshi's  on 9/24/15.
//  Copyright © 2015 Self. All rights reserved.
//

#import "CNGlobalViewController.h"
#import "CNFavViewController.h"
#import "CContactListViewController.h"
#import "CContactDetailsViewController.h"
#import "CNLogViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>


@interface CNGlobalViewController ()<UITabBarDelegate,UIAlertViewDelegate,MONActivityIndicatorViewDelegate>
{
    UITabBar *bottomTabBar;
}

@end

@implementation CNGlobalViewController

-(void)viewDidAppear:(BOOL)animated
{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    if ([self isKindOfClass:[ARTCVideoChatViewController class]])
//        return UIInterfaceOrientationMaskAll;
//    else
        return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate {
//    if ([self isKindOfClass:[ARTCVideoChatViewController class]])
//        return YES;
//    else
        return NO;
}

-(void)addTabBarToView:(UIView *)view

{
    
    UIView *containerView=[[[NSBundle mainBundle]loadNibNamed:@"Extended" owner:self options:nil]objectAtIndex:0];
    
    [containerView setFrame:CGRectMake(0, view.frame.size.height-49, view.frame.size.width, 49)];
    
    bottomTabBar=(UITabBar *)[containerView viewWithTag:100];
    
    [bottomTabBar setDelegate:self];
    
//    [bottomTabBar setValue:@(YES) forKeyPath:@"_hidesShadow"]; //for disable
    
//    for (UITabBarItem * item in  bottomTabBar.items)
//    {
//        
//        [item setImageInsets: UIEdgeInsetsMake(4, 0, -4, 0)];
//
//    }

    UIViewController *visibleVC=[self.navigationController visibleViewController];
    
    
    
    if ([visibleVC isKindOfClass:[CNFavViewController class]])
    {
        [bottomTabBar setSelectedItem: [bottomTabBar.items objectAtIndex:0]];
    }
    else if ([visibleVC isKindOfClass:[CContactListViewController class]] || [visibleVC isKindOfClass:[CContactDetailsViewController class]])
    {
        [bottomTabBar setSelectedItem: [bottomTabBar.items objectAtIndex:1]];
    }
    else if ([visibleVC isKindOfClass:[CNLogViewController class]])
    {
        [bottomTabBar setSelectedItem:[bottomTabBar.items objectAtIndex:2]];
    }
    [view addSubview:containerView];
}

#pragma mark UITabBarDelegate


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    NSInteger selectedTag = tabBar.selectedItem.tag;
    
    DebugLog(@"%ld",(long)selectedTag);
    UIViewController *visibleVC=[self.navigationController visibleViewController];
    
    if (selectedTag == 0)
    {
        
        DebugLog(@"tab 1");
        
        //Do what ever you want here
        if (![visibleVC isKindOfClass:[CNFavViewController class]])
        {
            CNFavViewController *CTPVC=[[UIStoryboard storyboardWithName:@"IPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"CNFavViewController"];
            
            [self PushViewController:CTPVC WithAnimation:kCAMediaTimingFunctionEaseIn];
        }
        
    } else if(selectedTag == 1)
        
    {
        
        DebugLog(@"tab 2");
        
        //Do what ever you want
        if (![visibleVC isKindOfClass:[CContactListViewController class]])
        {
            CContactListViewController *CTPVC=[[UIStoryboard storyboardWithName:@"IPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"CContactListViewController"];
            
            [self PushViewController:CTPVC WithAnimation:kCAMediaTimingFunctionEaseIn];
        }
    }
    
    else if(selectedTag == 2)
        
    {
        
        DebugLog(@"tab 3");
        if (![visibleVC isKindOfClass:[CNLogViewController class]])
        {
            CNLogViewController *CTPVC=[[UIStoryboard storyboardWithName:@"IPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"CNLogViewController"];
            
            [self PushViewController:CTPVC WithAnimation:kCAMediaTimingFunctionEaseIn];
        }
        //Do what ever you want here
        
    }
}

// loader works here
-(void)startLoadingOnView:(UIView *)view
{
    [indicatorView removeFromSuperview];
    indicatorView=nil;
    indicatorView = [[MONActivityIndicatorView alloc] init];
    indicatorView.delegate = self;
    indicatorView.numberOfCircles = 4;
    indicatorView.radius = 10;
    indicatorView.internalSpacing = 3;
    [indicatorView startAnimating];
    
    [view addSubview:indicatorView];
    [self placeAtTheCenterWithView:indicatorView on:view]; 
//    [SVProgressHUD showWithStatus:@"Loading please wait.."];
//    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:52.0f/255.0f green:151.0f/255.0f blue:250.0f/255.0f alpha:1]];
}
-(void)stopLoading
{
    [indicatorView stopAnimating];
//    [SVProgressHUD dismiss];
}

- (void)placeAtTheCenterWithView:(UIView *)view on:(UIView *)onView {
    [onView addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                       attribute:NSLayoutAttributeCenterX
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:onView
                                                       attribute:NSLayoutAttributeCenterX
                                                      multiplier:1.0f
                                                        constant:0.0f]];
    
    [onView addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                       attribute:NSLayoutAttributeCenterY
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:onView
                                                       attribute:NSLayoutAttributeCenterY
                                                      multiplier:1.0f
                                                        constant:0.0f]];
}
- (UIColor *)activityIndicatorView:(MONActivityIndicatorView *)activityIndicatorView
      circleBackgroundColorAtIndex:(NSUInteger)index {
    if (index%2==0)
        return APPBLUECOLOR;
    else
        return [UIColor blackColor];
}

-(NSString *)trim:(NSString *)str
{
    NSString *trimmedString = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return trimmedString;
    
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    if ([emailTest evaluateWithObject:checkString])
    {
        for (int index=0; index<checkString.length-2; index++)
        {
            char sub_char = [checkString characterAtIndex:index];
            if ((int)sub_char>=65 && (int)sub_char<=90)
            {
                
            }
            else if ((int)sub_char>=97 && (int)sub_char<=122)
            {
                
            }
            else if ((int)sub_char>=48 && (int)sub_char<=57)
            {
                
            }
            else
            {
                char sub_char1 = [checkString characterAtIndex:index+1];
                
                if ((int)sub_char1>=65 && (int)sub_char1<=90)
                {
                    
                }
                else if ((int)sub_char1>=97 && (int)sub_char1<=122)
                {
                    
                }
                else if ((int)sub_char1>=48 && (int)sub_char1<=57)
                {
                    
                }
                else
                {
                    return NO;
                }
            }
        }
        return [emailTest evaluateWithObject:checkString];
    }
    else
    {
        return [emailTest evaluateWithObject:checkString];
    }
}


-(void)setRoundCornertoView:(UIView *)viewName withBorderColor:(UIColor *)color borderWidth:(CGFloat)borderWidth WithRadius:(CGFloat)radius dependsOnHeight:(BOOL)dependsOnHeight
{
    if (dependsOnHeight)
        viewName.layer.cornerRadius=viewName.frame.size.height*radius;
    else
        viewName.layer.cornerRadius=radius;
    viewName.layer.borderWidth=borderWidth;
    viewName.layer.borderColor=color.CGColor;
    [viewName.layer setMasksToBounds:YES];
}

-(void)setBorderToView:(UIView *)viewName withColor:(UIColor *)color withBorderWidth:(CGFloat)width
{
    viewName.layer.borderWidth=width;
    viewName.layer.borderColor=color.CGColor;
    [viewName.layer setMasksToBounds:YES];
}


- (CGSize)sizeOfTextView:(UITextView *)textView withText:(NSString *)text {
    
    CGSize maximumLabelSize = CGSizeMake(textView.frame.size.width, 2000);
    CGRect textRect=[text boundingRectWithSize:maximumLabelSize
                                       options:NSStringDrawingUsesFontLeading|NSLineBreakByWordWrapping|NSLineBreakByCharWrapping
                     |NSStringDrawingUsesLineFragmentOrigin
                                    attributes:@{NSFontAttributeName:textView.font}
                                       context:nil];
    return textRect.size;
}

- (CGSize)sizeOfLabelwithFont:(UIFont *)font withText:(NSString *)text ofWidth:(CGFloat)width {
    CGSize maximumLabelSize = CGSizeMake(FULLWIDTH*(width/320), 999);
    CGRect textRect=[text boundingRectWithSize:maximumLabelSize
                                       options:NSStringDrawingUsesLineFragmentOrigin|NSLineBreakByWordWrapping|NSLineBreakByCharWrapping
                                    attributes:@{NSFontAttributeName:font}
                                       context:nil];
    return textRect.size;
}

-(void)getJsonResponse : (NSString *)urlStr withPostData:(id)postParam success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postParam length]];
    
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    NSData *postData;
    if (postParam != nil)
    {
        if ([postParam isKindOfClass:[NSString class]])
        {
            postData=[postParam dataUsingEncoding:NSUTF8StringEncoding];
        }
        else
            postData = [NSJSONSerialization dataWithJSONObject:postParam options:0 error:&error];
    }
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    self.connectionSession = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           if (error==nil && data !=nil)
                                           {
                                               NSString *str=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                                               NSDictionary * json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               DebugLog(@"session returned data-%@",str);
                                               
                                               success(json);
                                           }
                                           else
                                               failure(error);
                                           
                                       }];
    
    [self.connectionSession resume] ;
}

-(void)textFieldErrorNotify:(UITextField *)textField
{
    UIColor *placeholder = [textField valueForKeyPath:@"_placeholderLabel.textColor"];
    UIColor *textColor = [textField textColor];
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"position.x";
    animation.values = @[ @0, @10, @-10, @10, @0 ];
    animation.keyTimes = @[ @0, @(1 / 6.0), @(3 / 6.0), @(5 / 6.0), @1 ];
    animation.duration = 0.3;
    
    animation.additive = YES;
    
    [textField.layer addAnimation:animation forKey:@"shake"];
    [textField setTextColor:[UIColor redColor]];
    [textField setValue:[UIColor redColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [textField setTextColor:textColor];
        [textField setValue:placeholder forKeyPath:@"_placeholderLabel.textColor"];
    });
}

// dynamic label text size depending on device screensize

-(CGFloat)setFontSize:(CGFloat)fontSize
{
    if (IsIphone4)
        return fontSize;
    else if (IsIphone5)
        return fontSize+1;
    else if (IsIphone6)
        return fontSize+2;
    else if (IsIphone6plus)
        return fontSize+3;
    else
        return fontSize;
}

// custom push method with custom animation

-(void)PushViewController:(UIViewController *)viewController WithAnimation:(NSString *)AnimationType
{
    CATransition *Transition=[CATransition animation];
    [Transition setDuration:0.1f];
    [Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [Transition setType:AnimationType];
    [[[[self navigationController] view] layer] addAnimation:Transition forKey:nil];
    [[self navigationController] pushViewController:viewController animated:NO];
}

// custom pop method with custom animation

-(void)popToPreviousViewController:(id)className
{
    if (className == nil)
    {
        CATransition *Transition=[CATransition animation];
        [Transition setDuration:0.1f];
        [Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [Transition setType:kCAMediaTimingFunctionEaseOut];
        [[[[self navigationController] view] layer] addAnimation:Transition forKey:nil];
        [[self navigationController] popViewControllerAnimated:NO];
    }
    else
    {
        for (int i=(int)self.navigationController.viewControllers.count-1; i>=0; i--)
        {
            UIViewController *vc = self.navigationController.viewControllers[i];
            if ([NSStringFromClass([vc class]) isEqualToString:className])
            {
                CATransition *Transition=[CATransition animation];
                [Transition setDuration:0.1f];
                [Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
                [Transition setType:kCAMediaTimingFunctionEaseOut];
                [[[[self navigationController] view] layer] addAnimation:Transition forKey:nil];
                [[self navigationController] popToViewController:vc animated:NO];
                break;
            }
        }
    }
}

// sending push to user through user id

-(void)sendPushToUser:(NSString *)receiverID withRoomID:(NSString *)roomID withCallType:(NSString *)callType withStatus:(NSString *)status
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSString *urlString=[NSString stringWithFormat:@"%@/push_send_v2?sender_id=%@&receiver_id=%@&roomId=%@&mod=%@&pem_mod=%@&status=%@",DOMAINURL,[[NSUserDefaults standardUserDefaults] objectForKey:CNUSERID],receiverID,roomID,callType,PUSHMODE,status];
        
        DebugLog(@"create session %@",urlString);
        
        NSError *localErr;
        
        NSHTTPURLResponse *response = nil;
        NSURL *url = [NSURL URLWithString:urlString];
        
        //-- Get request and response though URL
        NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
        
        //-- JSON Parsing
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&localErr];
        NSLog(@"Result = %@",result);
        
        if (result != nil)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([result isKindOfClass:[NSDictionary class]] && [[result objectForKey:@"response"] isEqualToString:@"TRUE"])
                {   }
                else
                {   }
            });
        }
    });
}

// -- core dara works here -- //

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

// adding log to local db containing name,phn,date etc

-(void)addToCallLogWithName:(NSString *)name phone:(NSString *)phone typeOfCall:(NSNumber *)calltype time:(NSDate *)time date:(NSString *)date fullDate:(NSDate *)dateString
{
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"CallLog"];
    
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"phone==%@ AND date==%@ AND callType==%@",phone, date,calltype];

    fetchRequest.predicate=predicate;
    
    NSArray *userArray=[self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    NSManagedObject *user;
    if (userArray != nil)
        user=[userArray firstObject];
    if (user == nil)
    {
            NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
            [managedObjectContext performBlock:^{
                NSManagedObject *newObject = [NSEntityDescription insertNewObjectForEntityForName:@"CallLog" inManagedObjectContext:managedObjectContext];
                [newObject setValue:calltype forKey:@"callType"];
                [newObject setValue:date forKey:@"date"];
                [newObject setValue:dateString forKey:@"dateString"];
                [newObject setValue:name forKey:@"name"];
                [newObject setValue:phone forKey:@"phone"];
                [newObject setValue:time forKey:@"time"];
                [newObject setValue:[NSString stringWithFormat:@"%d",1] forKey:@"numberOfCall"];
                NSError *error = nil;
                if ([managedObjectContext save:&error])
                {   }
            }];
    }
    else //if ([user count] >= 1)
    {
        int numberOfCall = [[user valueForKey:@"numberOfCall"] intValue];
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        [managedObjectContext performBlock:^{
            
            [user setValue:[NSString stringWithFormat:@"%d",numberOfCall+1] forKey:@"numberOfCall"];
            [user setValue:dateString forKey:@"dateString"];
            NSError *error = nil;
            if ([managedObjectContext save:&error])
            {}
        }];
    }
//    if ([[self.navigationController visibleViewController] isKindOfClass:[CNLogViewController class]])
//    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"CallLogUpdated" object:nil userInfo:nil];
//    }
}

#pragma mark -- camera,microphone permission 

-(NSString *)checkStatusForMicrophoneAndCameraPermission
{
    if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio]==AVAuthorizationStatusAuthorized)
    {
        if ([AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo]==AVAuthorizationStatusAuthorized)
        {
            return @"OK";
        }
        else
            return @"AVVIDEO";
    }
    else
    {
        return @"AVAUDIO";
    }
    
}

// alert for need microphone and camera permission before creating any meeting

-(void)showAlertForCameraAndMicrophonePermissions
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Warning!!!"
                                          message:[NSString stringWithFormat:@"To use all features of this app you need to allow camera and microphone permissions from settings."]
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                                   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                               }];
    [alertController addAction:okAction];
    NSLog(@"visible view %@",self.navigationController);
    UINavigationController *nav=(UINavigationController *)[[[UIApplication sharedApplication]keyWindow] rootViewController];
    [nav.visibleViewController presentViewController:alertController animated:YES completion:nil];
}

#pragma mark -- coretelephony work --

// cheching is there any normal cellular call or not

-(BOOL)isOnCall
{
    CTCallCenter *ctCallCenter = [[CTCallCenter alloc] init];
    if (ctCallCenter.currentCalls != nil)
    {
        NSArray* currentCalls = [ctCallCenter.currentCalls allObjects];
        for (CTCall *call in currentCalls)
        {
            if(call.callState == CTCallStateConnected || call.callState == CTCallStateIncoming || call.callState == CTCallStateDialing)
            {
                return YES;
            }
            return NO;
        }
    }
    return NO;
}

@end