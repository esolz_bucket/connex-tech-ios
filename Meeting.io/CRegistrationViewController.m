//
//  CRegistrationViewController.m
//  Connex-New
//
//  Created by Saptarshi's  on 6/27/16.
//  Copyright © 2016 esolz. All rights reserved.
//

#import "CRegistrationViewController.h"
#import "CContactListViewController.h"
#import <DigitsKit/DigitsKit.h>

@interface CRegistrationViewController ()
{
    UIToolbar* numberToolbar;
    NSOperationQueue *opertaionQueue;
}
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UILabel *welcomeLBL;
@property (weak, nonatomic) IBOutlet UILabel *signinLBL;
@property (weak, nonatomic) IBOutlet UILabel *connexLBL;
@property (weak, nonatomic) IBOutlet UIButton *nextBTN;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;

@end

@implementation CRegistrationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    opertaionQueue = [[NSOperationQueue alloc]init];
    [_welcomeLBL setFont:[UIFont fontWithName:_welcomeLBL.font.fontName size:[self setFontSize:_welcomeLBL.font.pointSize]]];
    [_signinLBL setFont:[UIFont fontWithName:_signinLBL.font.fontName size:[self setFontSize:_signinLBL.font.pointSize]]];
    [_connexLBL setFont:[UIFont fontWithName:_connexLBL.font.fontName size:[self setFontSize:_connexLBL.font.pointSize]]];
    
    [_nextBTN.titleLabel setFont:[UIFont fontWithName:_nextBTN.titleLabel.font.fontName size:[self setFontSize:_nextBTN.titleLabel.font.pointSize]]];
    
    NSDictionary *meetingAttrDict = @{
                                      NSFontAttributeName : [UIFont systemFontOfSize:[self setFontSize:_connexLBL.font.pointSize]+15],
                                      NSForegroundColorAttributeName : HEADER_COLOR
                                      };
    NSMutableAttributedString *meetingAttStr = [[NSMutableAttributedString alloc] initWithString:@"Connex" attributes: meetingAttrDict];
    
    NSDictionary *toAttrDict = @{
                                 NSFontAttributeName : [UIFont systemFontOfSize:_connexLBL.font.pointSize],
                                 NSForegroundColorAttributeName : [UIColor blackColor]
                                 };
    NSMutableAttributedString *toAttStr = [[NSMutableAttributedString alloc] initWithString:@"" attributes: toAttrDict];
    [toAttStr appendAttributedString:meetingAttStr];
    [_connexLBL setAttributedText:toAttStr];
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    UIBarButtonItem *doneBTN=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)];
    [doneBTN setTintColor:[UIColor whiteColor]];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],doneBTN];
    [numberToolbar sizeToFit];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

- (IBAction)register:(id)sender
{
    [self.view endEditing:YES];
    [_mainScroll setContentOffset:CGPointMake(0,0) animated:YES];
    if ([[_nameTF text]length]>0)
    {
        if ([[_emailTF text]length]>0)
        {
            if ([self NSStringIsValidEmail:_emailTF.text])
            {
                [[Digits sharedInstance] logOut];
                [[Digits sharedInstance] authenticateWithCompletion:^
                 (DGTSession* session, NSError *error) {
                     if (session) {
                         // Inspect session/error objects
                         DebugLog(@"digits session---  %@ %@ == %@",session.authTokenSecret,session.authToken, session.phoneNumber);
                         if ([session.phoneNumber length]>0)
                         {
                             
                             NSString *trimmedString=[[NSString stringWithFormat:@"%@",session.phoneNumber] substringFromIndex:MAX((int)[session.phoneNumber length]-10, 0)]; //in case string is less than 4 characters long.
                             
                             [self startLoadingOnView:self.view];
                             [opertaionQueue addOperationWithBlock:^{
                                 [self didRegisterWithNumber:trimmedString withName:[_nameTF text] withEmail:[_emailTF text]];
                             }];
                         }
                     }
                     else
                     {
                         DebugLog(@"hdchb %@",error);
                     }
                 }];
            }
            else
            {
                UIAlertController *alertController = [UIAlertController
                                                      alertControllerWithTitle:@"Alert!"
                                                      message:[NSString stringWithFormat:@"Enter a valid email"]
                                                      preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                           }];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        else
        {
            [[Digits sharedInstance] logOut];
            [[Digits sharedInstance] authenticateWithCompletion:^
             (DGTSession* session, NSError *error) {
                 if (session) {
                     // Inspect session/error objects
                     DebugLog(@"digits session---  %@ %@ == %@",session.authTokenSecret,session.authToken, session.phoneNumber);
                     if ([session.phoneNumber length]>0)
                     {
                         
                         NSString *trimmedString=[[NSString stringWithFormat:@"%@",session.phoneNumber] substringFromIndex:MAX((int)[session.phoneNumber length]-10, 0)]; //in case string is less than 4 characters long.
                         
                         [self startLoadingOnView:self.view];
                         [opertaionQueue addOperationWithBlock:^{
                             [self didRegisterWithNumber:trimmedString withName:[_nameTF text] withEmail:@""];
                         }];
                     }
                 }
                 else
                 {
                     DebugLog(@"hdchb %@",error);
                 }
             }];
        }
    }
    else
    {
        [self textFieldErrorNotify:_nameTF];
    }
}

-(void)didRegisterWithNumber:(NSString *)phone withName:(NSString *)name withEmail:(NSString *)email
{
    NSArray *keys = [NSArray arrayWithObjects:@"name", @"phone", @"email",@"device_token_ios",@"device_token_android", nil];
    
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults]objectForKey:CNDEVICETOKEN];
    if (deviceToken == nil || [deviceToken isKindOfClass:[NSNull class]])
    {
        deviceToken=@"";
    }
    
    NSArray *objects = [NSArray arrayWithObjects:[name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]], [phone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]], [email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],deviceToken,@"", nil];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    NSMutableArray *finalDict = [[NSMutableArray alloc]init];
    
    //    [finalDict setObject:dictionary forKey:@"details_info"];
    [finalDict addObject:dictionary];
    
    NSError *localErr;
    
    NSHTTPURLResponse *response = nil;
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:finalDict options:NSJSONWritingPrettyPrinted error:&localErr];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    
    DebugLog(@"json string ertta: %@",jsonString);
    
    NSMutableString *urlString=[NSMutableString stringWithFormat:@"%@/login?details_info=%@",DOMAINURL,[self encodeToPercentEscapeString:jsonString]];
    
    NSLog(@"url string %@",urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    //-- Get request and response though URL
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    //-- JSON Parsing
    NSMutableDictionary *result;
    if (responseData != nil)
        result = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self stopLoading];
        });
    }
    NSLog(@"Result = %@",result);
    if (result != nil)
    {
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            if ([[result objectForKey:@"response"]isEqualToString:@"TRUE"])
            {
                [self stopLoading];
                [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",[result valueForKey:@"user_id"]] forKey:CNUSERID];
                [[NSUserDefaults standardUserDefaults]setObject:email forKey:CNEMAIL];
                [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",phone] forKey:CNPHONE];
                [[NSUserDefaults standardUserDefaults]setObject:name forKey:CNUSERNAME];
                CContactListViewController *CCDVC=[[UIStoryboard storyboardWithName:@"IPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"CContactListViewController"];
                [self PushViewController:CCDVC WithAnimation:kCAMediaTimingFunctionEaseIn];
            }
            else
            {
                [self stopLoading];
            }
        }];
    }
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string
{
    return [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    if (textField  == _phoneTF)
//        textField.inputAccessoryView = numberToolbar;
    [_mainScroll setContentOffset:CGPointMake(0.0f,180) animated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self.mainScroll setContentOffset:CGPointMake(0,0) animated:YES];
    return YES;
}

-(void)doneWithNumberPad
{
    [self.mainScroll setContentOffset:CGPointMake(0,0) animated:YES];
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
