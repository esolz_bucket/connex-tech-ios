//
//  CNLandingViewController.h
//  Meeting.io
//
//  Created by Saptarshi's  on 8/11/16.
//  Copyright © 2016 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNGlobalViewController.h"
#import "AppDelegate.h"

@interface CNLandingViewController : CNGlobalViewController

@end
