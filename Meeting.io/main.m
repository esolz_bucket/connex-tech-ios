//
//  main.m
//  Meeting.io
//
//  Created by esolz on 18/05/16.
//  Copyright © 2016 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
